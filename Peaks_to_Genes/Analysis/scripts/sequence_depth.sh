#! /bin/bash

#SBATCH -n 2
#SBATCH -t 168:00:00
#SBATCH --mem=MaxMemPerNode
#SBATCH -J 'sequence_depth'
#SBATCH -o /users/ahadik/scratch/working/sequence_depth-300.out
#SBATCH -e /users/ahadik/scratch/working/sequence_depth-300.err
#SBATCH --qos='ccmb-condo'
#SBATCH --mail-type=ALL
#SBATCH --mail-user=alexander_hadik@brown.edu

cd ~/data/jdobson/PeaksToGenes/bin

module load bedtools

perl sequenceDepthScale.pl --step 300 --input ~/data/jdobson/working_directory/ChIP-seq/Mapped_Reads/Combined_replicates/Converted_Reads_in_BED_Format/CG1832_Input_Kc_Cells_GFP_RNAi_Combined_converted.bed --ip ~/data/jdobson/working_directory/ChIP-seq/Mapped_Reads/Combined_replicates/Converted_Reads_in_BED_Format/CG1832_ChIP_Kc_Cells_GFP_RNAi_Combined_converted.bed --chromosome_sizes ~/data/jdobson/PeaksToGenes/chromosome_sizes/dm3.chrom.sizes --processors 2

perl sequenceDepthScale.pl --step 300 --input ~/data/jdobson/working_directory/ChIP-seq/Mapped_Reads/Combined_replicates/Converted_Reads_in_BED_Format/CG1832_Input_S2_Cells_GPF_RNAi_Combined_converted.bed --ip ~/data/jdobson/working_directory/ChIP-seq/Mapped_Reads/Combined_replicates/Converted_Reads_in_BED_Format/CG1832_ChIP_S2_Cells_GFP_RNAi_Combined_converted.bed --chromosome_sizes ~/data/jdobson/PeaksToGenes/chromosome_sizes/dm3.chrom.sizes --processors 2

perl sequenceDepthScale.pl --step 300 --input ~/data/jdobson/working_directory/ChIP-seq/Mapped_Reads/Combined_replicates/Converted_Reads_in_BED_Format/CG1832_Input_S2_Cells_MSL2_RNAi_Combined_converted.bed --ip ~/data/jdobson/working_directory/ChIP-seq/Mapped_Reads/Combined_replicates/Converted_Reads_in_BED_Format/CG1832_ChIP_S2_Cells_MSL2_RNAi_Combined_converted.bed --chromosome_sizes ~/data/jdobson/PeaksToGenes/chromosome_sizes/dm3.chrom.sizes --processors 2

perl sequenceDepthScale.pl --step 300 --input ~/data/jdobson/working_directory/ChIP-seq/Mapped_Reads/Combined_replicates/Converted_Reads_in_BED_Format/MSL_Input_S2_Cells_Combined_converted.bed --ip ~/data/jdobson/working_directory/ChIP-seq/Mapped_Reads/Combined_replicates/Converted_Reads_in_BED_Format/MSL_ChIP_S2_Cells_Combined_converted.bed --chromosome_sizes ~/data/jdobson/PeaksToGenes/chromosome_sizes/dm3.chrom.sizes --processors 2