#! /bin/bash

#SBATCH -n 2
#SBATCH -t 168:00:00
#SBATCH --mem=MaxMemPerNode
#SBATCH -J 'peakstogenes-contrast-reg-v-unreg'
#SBATCH -o /users/ahadik/scratch/working/peakstogenes-contrast-reg-v-unreg.out
#SBATCH -e /users/ahadik/scratch/working/peakstogenes-contrast-reg-v-unreg.err
#SBATCH --qos='ccmb-condo'
#SBATCH --mail-type=ALL
#SBATCH --mail-user=alexander_hadik@brown.edu

#CD into PeaksToGenes installation to execute script
cd ~/data/jdobson/PeaksToGenes/bin

#load bedtools
module load bedtools

#X Chromosome analysis TEST: regulated x chrom genes BACKGROUND: unregulated x chrom genes

#Kc Cells CLAMP RNAi
perl peaksToGenes.pl --contrast --genome dm3 --name Kc_Cells_GFP --contrast_name CG1832_ChIP_Kc_Cells_GFP_RNAi_contrast_Kc_CLAMP_RNAi_xChrom-reg-v-unreg_1-2_cutoff --test_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-01-13/kccells_clamp_rnai_paired_rep1_s_8/regulated_xChrom_1.2-fold_change.csv --background_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-01-13/kccells_clamp_rnai_paired_rep1_s_8/unregulated_xChrom_1.2-fold_change.csv

perl peaksToGenes.pl --contrast --genome dm3 --name Kc_Cells_GFP --contrast_name CG1832_ChIP_Kc_Cells_GFP_RNAi_contrast_Kc_CLAMP_RNAi_xChrom-reg-v-unreg_2-0_cutoff --test_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-01-13/kccells_clamp_rnai_paired_rep1_s_8/regulated_xChrom_2.0-fold_change.csv --background_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-01-13/kccells_clamp_rnai_paired_rep1_s_8/unregulated_xChrom_2.0-fold_change.csv



#S2 CLAMP RNAi
perl peaksToGenes.pl --contrast --genome dm3 --name Kc_Cells_GFP --contrast_name CG1832_ChIP_Kc_Cells_GFP_RNAi_contrast_S2_CLAMP_RNAi_xChrom-reg-v-unreg_1-2_cutoff --test_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-01-13/s2cells_clamp_rnai_paired_rep1_s_2/regulated_xChrom_1.2-fold_change.csv --background_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-01-13/s2cells_clamp_rnai_paired_rep1_s_2/unregulated_xChrom_1.2-fold_change.csv

perl peaksToGenes.pl --contrast --genome dm3 --name Kc_Cells_GFP --contrast_name CG1832_ChIP_Kc_Cells_GFP_RNAi_contrast_S2_CLAMP_RNAi_xChrom-reg-v-unreg_2-0_cutoff --test_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-01-13/s2cells_clamp_rnai_paired_rep1_s_2/regulated_xChrom_2.0-fold_change.csv --background_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-01-13/s2cells_clamp_rnai_paired_rep1_s_2/unregulated_xChrom_2.0-fold_change.csv



#S2 MSL2 RNAi
perl peaksToGenes.pl --contrast --genome dm3 --name Kc_Cells_GFP --contrast_name CG1832_ChIP_Kc_Cells_GFP_RNAi_contrast_S2_MSL2_RNAi_xChrom-reg-v-unreg_1-2_cutoff --test_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-01-13/s2cells_msl2_rnai_paired_rep1_s_3/regulated_xChrom_1.2-fold_change.csv --background_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-01-13/s2cells_msl2_rnai_paired_rep1_s_3/unregulated_xChrom_1.2-fold_change.csv

perl peaksToGenes.pl --contrast --genome dm3 --name Kc_Cells_GFP --contrast_name CG1832_ChIP_Kc_Cells_GFP_RNAi_contrast_S2_MSL2_RNAi_xChrom-reg-v-unreg_2-0_cutoff --test_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-01-13/s2cells_msl2_rnai_paired_rep1_s_3/regulated_xChrom_2.0-fold_change.csv --background_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-01-13/s2cells_msl2_rnai_paired_rep1_s_3/unregulated_xChrom_2.0-fold_change.csv


#S2 KC Combined
perl peaksToGenes.pl --contrast --genome dm3 --name Kc_Cells_GFP --contrast_name CG1832_ChIP_Kc_Cells_GFP_RNAi_contrast_S2_MSL2_RNAi_xChrom-reg-v-unreg_1-2_cutoff --test_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-09-13/kc_s2_combined_clamp_rnai_paired/regulated_xChrom_1.2-fold_change.csv --background_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-09-13/kc_s2_combined_clamp_rnai_paired/unregulated_xChrom_1.2-fold_change.csv

perl peaksToGenes.pl --contrast --genome dm3 --name Kc_Cells_GFP --contrast_name CG1832_ChIP_Kc_Cells_GFP_RNAi_contrast_S2_MSL2_RNAi_xChrom-reg-v-unreg_2-0_cutoff --test_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-09-13/kc_s2_combined_clamp_rnai_paired/regulated_xChrom_2.0-fold_change.csv --background_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-09-13/kc_s2_combined_clamp_rnai_paired/unregulated_xChrom_2.0-fold_change.csv







#AUTOSOMEanalysis TEST: regulated autosomal genes BACKGROUND: unregulated autosomal genes

#Kc Cells CLAMP RNAi
perl peaksToGenes.pl --contrast --genome dm3 --name Kc_Cells_GFP --contrast_name CG1832_ChIP_Kc_Cells_GFP_RNAi_contrast_Kc_CLAMP_RNAi_autosome-reg-v-unreg_1-2_cutoff --test_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-01-13/kccells_clamp_rnai_paired_rep1_s_8/regulated_autosome_1.2-fold_change.csv --background_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-01-13/kccells_clamp_rnai_paired_rep1_s_8/unregulated_autosome_1.2-fold_change.csv

perl peaksToGenes.pl --contrast --genome dm3 --name Kc_Cells_GFP --contrast_name CG1832_ChIP_Kc_Cells_GFP_RNAi_contrast_Kc_CLAMP_RNAi_autosome-reg-v-unreg_2-0_cutoff --test_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-01-13/kccells_clamp_rnai_paired_rep1_s_8/regulated_autosome_2.0-fold_change.csv --background_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-01-13/kccells_clamp_rnai_paired_rep1_s_8/unregulated_autosome_2.0-fold_change.csv



#S2 CLAMP RNAi
perl peaksToGenes.pl --contrast --genome dm3 --name Kc_Cells_GFP --contrast_name CG1832_ChIP_Kc_Cells_GFP_RNAi_contrast_S2_CLAMP_RNAi_autosome-reg-v-unreg_1-2_cutoff --test_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-01-13/s2cells_clamp_rnai_paired_rep1_s_2/regulated_autosome_1.2-fold_change.csv --background_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-01-13/s2cells_clamp_rnai_paired_rep1_s_2/unregulated_autosome_1.2-fold_change.csv

perl peaksToGenes.pl --contrast --genome dm3 --name Kc_Cells_GFP --contrast_name CG1832_ChIP_Kc_Cells_GFP_RNAi_contrast_S2_CLAMP_RNAi_autosome-reg-v-unreg_2-0_cutoff --test_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-01-13/s2cells_clamp_rnai_paired_rep1_s_2/regulated_autosome_2.0-fold_change.csv --background_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-01-13/s2cells_clamp_rnai_paired_rep1_s_2/unregulated_autosome_2.0-fold_change.csv



#S2 MSL2 RNAi
perl peaksToGenes.pl --contrast --genome dm3 --name Kc_Cells_GFP --contrast_name CG1832_ChIP_Kc_Cells_GFP_RNAi_contrast_S2_MSL2_RNAi_autosome-reg-v-unreg_1-2_cutoff --test_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-01-13/s2cells_msl2_rnai_paired_rep1_s_3/regulated_autosome_1.2-fold_change.csv --background_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-01-13/s2cells_msl2_rnai_paired_rep1_s_3/unregulated_autosome_1.2-fold_change.csv

perl peaksToGenes.pl --contrast --genome dm3 --name Kc_Cells_GFP --contrast_name CG1832_ChIP_Kc_Cells_GFP_RNAi_contrast_S2_MSL2_RNAi_autosome-reg-v-unreg_2-0_cutoff --test_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-01-13/s2cells_msl2_rnai_paired_rep1_s_3/regulated_autosome_2.0-fold_change.csv --background_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-01-13/s2cells_msl2_rnai_paired_rep1_s_3/unregulated_autosome_2.0-fold_change.csv


#S2 KC Combined
perl peaksToGenes.pl --contrast --genome dm3 --name Kc_Cells_GFP --contrast_name CG1832_ChIP_Kc_Cells_GFP_RNAi_contrast_S2_KC_Combined_autosome-reg-v-unreg_1-2_cutoff --test_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-09-13/kc_s2_combined_clamp_rnai_paired/regulated_autosome_1.2-fold_change.csv --background_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-09-13/kc_s2_combined_clamp_rnai_paired/unregulated_autosome_1.2-fold_change.csv

perl peaksToGenes.pl --contrast --genome dm3 --name Kc_Cells_GFP --contrast_name CG1832_ChIP_Kc_Cells_GFP_RNAi_contrast_S2_KC_Combined_autosome-reg-v-unreg_2-0_cutoff --test_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-09-13/kc_s2_combined_clamp_rnai_paired/regulated_autosome_2.0-fold_change.csv --background_genes ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-09-13/kc_s2_combined_clamp_rnai_paired/unregulated_autosome_2.0-fold_change.csv