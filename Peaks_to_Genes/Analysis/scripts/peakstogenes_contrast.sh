#! /bin/bash

#SBATCH -n 2
#SBATCH -t 168:00:00
#SBATCH --mem=MaxMemPerNode
#SBATCH -J 'peakstogenes-contrast'
#SBATCH -o /users/ahadik/scratch/working/peakstogenes-contrast.out
#SBATCH -e /users/ahadik/scratch/working/peakstogenes-contrast.err
#SBATCH --qos='ccmb-condo'
#SBATCH --mail-type=ALL
#SBATCH --mail-user=alexander_hadik@brown.edu

#CD into PeaksToGenes installation to execute script
cd ~/data/jdobson/PeaksToGenes/bin

#load bedtools
module load bedtools


#Contrast CLAMP Binding in Kc Cells with GFP RNAi to genes in S2 Cells that have a 1.2 fold change in response to MSL2 RNAi
perl peaksToGenes.pl --contrast --genome dm3 --name Kc_Cells_GFP-300-step --contrast_name CG1832_ChIP_Kc_Cells_GFP_RNAi_contrast_S2_MSL2_1-2_cutoff --test_genes /users/ahadik/data/jdobson/working_directory/RNA-seq/Analysis/Gene_Lists/parsed-data/s2cells_msl2_paired_rep1_s_3/s2cells_msl2_rnai_paired_rep1_s_3-gene_list_-1-2_fold-change.csv

#Contrast CLAMP Binding in Kc Cells with GFP RNAi to genes in S2 Cells that have a 2.0 fold change in response to MSL2 RNAi
perl peaksToGenes.pl --contrast --genome dm3 --name Kc_Cells_GFP-300-step --contrast_name CG1832_ChIP_Kc_Cells_GFP_RNAi_contrast_S2_MSL2_2-0_cutoff --test_genes /users/ahadik/data/jdobson/working_directory/RNA-seq/Analysis/Gene_Lists/parsed-data/s2cells_msl2_paired_rep1_s_3/s2cells_msl2_rnai_paired_rep1_s_3-gene_list_-2_fold-change.csv



#Contrast CLAMP Binding in S2 Cells with GFP RNAi to genes in S2 Cells that have a 1.2 fold change in response to MSL2 RNAi
perl peaksToGenes.pl --contrast --genome dm3 --name S2_Cells_GFP-300-step --contrast_name CG1832_ChIP_S2_Cells_GFP_RNAi_contrast_S2_MSL2_1-2_cutoff --test_genes /users/ahadik/data/jdobson/working_directory/RNA-seq/Analysis/Gene_Lists/parsed-data/s2cells_msl2_paired_rep1_s_3/s2cells_msl2_rnai_paired_rep1_s_3-gene_list_-1-2_fold-change.csv

#Contrast CLAMP Binding in S2 Cells with GFP RNAi to genes in S2 Cells that have a 2.0 fold change in response to MSL2 RNAi
perl peaksToGenes.pl --contrast --genome dm3 --name S2_Cells_GFP-300-step --contrast_name CG1832_ChIP_S2_Cells_GFP_RNAi_contrast_S2_MSL2_2-0_cutoff --test_genes /users/ahadik/data/jdobson/working_directory/RNA-seq/Analysis/Gene_Lists/parsed-data/s2cells_msl2_paired_rep1_s_3/s2cells_msl2_rnai_paired_rep1_s_3-gene_list_-2_fold-change.csv



#Contrast CLAMP Binding in S2 Cells with MSL2 RNAi to genes in S2 Cells that have a 1.2 fold change in response to MSL2 RNAi
perl peaksToGenes.pl --contrast --genome dm3 --name S2_Cells_MSL2-300-step --contrast_name CG1832_ChIP_S2_Cells_MSL2_RNAi_contrast_S2_MSL2_1-2_cutoff --test_genes /users/ahadik/data/jdobson/working_directory/RNA-seq/Analysis/Gene_Lists/parsed-data/s2cells_msl2_paired_rep1_s_3/s2cells_msl2_rnai_paired_rep1_s_3-gene_list_-1-2_fold-change.csv

#Contrast CLAMP Binding in S2 Cells with MSL2 RNAi to genes in S2 Cells that have a 2.0 fold change in response to MSL2 RNAi
perl peaksToGenes.pl --contrast --genome dm3 --name S2_Cells_MSL2-300-step --contrast_name CG1832_ChIP_S2_Cells_MSL2_RNAi_contrast_S2_MSL2_2-0_cutoff --test_genes /users/ahadik/data/jdobson/working_directory/RNA-seq/Analysis/Gene_Lists/parsed-data/s2cells_msl2_paired_rep1_s_3/s2cells_msl2_rnai_paired_rep1_s_3-gene_list_-2_fold-change.csv



#Contrast MSL2 Binding in S2 Cells to genes in S2 Cells that have a 1.2 fold change in response to MSL2 RNAi
perl peaksToGenes.pl --contrast --genome dm3 --name S2_Cells_MSL2_Combined-300-step --contrast_name MSL_ChIP_S2_Cells_contrast_S2_MSL2_1-2_cutoff --test_genes /users/ahadik/data/jdobson/working_directory/RNA-seq/Analysis/Gene_Lists/parsed-data/s2cells_msl2_paired_rep1_s_3/s2cells_msl2_rnai_paired_rep1_s_3-gene_list_-1-2_fold-change.csv

#Contrast MSL2 Binding in S2 Cells to genes in S2 Cells that have a 2.0 fold change in response to MSL2 RNAi
perl peaksToGenes.pl --contrast --genome dm3 --name S2_Cells_MSL2_Combined-300-step --contrast_name MSL_ChIP_S2_Cells_contrast_S2_MSL2_2-0_cutoff --test_genes /users/ahadik/data/jdobson/working_directory/RNA-seq/Analysis/Gene_Lists/parsed-data/s2cells_msl2_paired_rep1_s_3/s2cells_msl2_rnai_paired_rep1_s_3-gene_list_-2_fold-change.csv







#Contrast CLAMP Binding in Kc Cells with GFP RNAi to genes in S2 Cells that have a 1.2 fold change in response to CLAMP RNAi
perl peaksToGenes.pl --contrast --genome dm3 --name Kc_Cells_GFP-300-step --contrast_name CG1832_ChIP_Kc_Cells_GFP_RNAi_contrast_S2_CLAMP_1-2_cutoff --test_genes /users/ahadik/data/jdobson/working_directory/RNA-seq/Analysis/Gene_Lists/parsed-data/s2cells_clamp_paired_rep1_s_2/s2cells_clamp_rnai_paired_rep1_s_2-gene_list_-1-2_fold-change.csv

#Contrast CLAMP Binding in Kc Cells with GFP RNAi to genes in S2 Cells that have a 2.o fold change in response to CLAMP RNAi
perl peaksToGenes.pl --contrast --genome dm3 --name Kc_Cells_GFP-300-step --contrast_name CG1832_ChIP_Kc_Cells_GFP_RNAi_contrast_S2_CLAMP_2-0_cutoff --test_genes /users/ahadik/data/jdobson/working_directory/RNA-seq/Analysis/Gene_Lists/parsed-data/s2cells_clamp_paired_rep1_s_2/s2cells_clamp_rnai_paired_rep1_s_2-gene_list_-2_fold-change.csv



#Contrast CLAMP Binding in S2 Cells with GPF RNAi to genes in S2 Cells that have a 1.2 fold change in response to CLAMP RNAi
perl peaksToGenes.pl --contrast --genome dm3 --name S2_Cells_GFP-300-step --contrast_name CG1832_ChIP_S2_Cells_GFP_RNAi_contrast_S2_CLAMP_1-2_cutoff --test_genes /users/ahadik/data/jdobson/working_directory/RNA-seq/Analysis/Gene_Lists/parsed-data/s2cells_clamp_paired_rep1_s_2/s2cells_clamp_rnai_paired_rep1_s_2-gene_list_-1-2_fold-change.csv

#Contrast CLAMP Binding in S2 Cells with GPF RNAi to genes in S2 Cells that have a 2.0 fold change in response to CLAMP RNAi
perl peaksToGenes.pl --contrast --genome dm3 --name S2_Cells_GFP-300-step --contrast_name CG1832_ChIP_S2_Cells_GFP_RNAi_contrast_S2_CLAMP_2-0_cutoff --test_genes /users/ahadik/data/jdobson/working_directory/RNA-seq/Analysis/Gene_Lists/parsed-data/s2cells_clamp_paired_rep1_s_2/s2cells_clamp_rnai_paired_rep1_s_2-gene_list_-2_fold-change.csv



#Contrast CLAMP Binding in S2 Cells with MSL2 RNAi to genes in S2 Cells that have a 1.2 fold change in response to CLAMP RNAi
perl peaksToGenes.pl --contrast --genome dm3 --name S2_Cells_MSL2-300-step --contrast_name CG1832_ChIP_S2_Cells_MSL2_RNAi_contrast_S2_CLAMP_1-2_cutoff --test_genes /users/ahadik/data/jdobson/working_directory/RNA-seq/Analysis/Gene_Lists/parsed-data/s2cells_clamp_paired_rep1_s_2/s2cells_clamp_rnai_paired_rep1_s_2-gene_list_-1-2_fold-change.csv

#Contrast CLAMP Binding in S2 Cells with MSL2 RNAi to genes in S2 Cells that have a 2.0 fold change in response to CLAMP RNAi
perl peaksToGenes.pl --contrast --genome dm3 --name S2_Cells_MSL2-300-step --contrast_name CG1832_ChIP_S2_Cells_MSL2_RNAi_contrast_S2_CLAMP_2-0_cutoff --test_genes /users/ahadik/data/jdobson/working_directory/RNA-seq/Analysis/Gene_Lists/parsed-data/s2cells_clamp_paired_rep1_s_2/s2cells_clamp_rnai_paired_rep1_s_2-gene_list_-2_fold-change.csv



#Contrast MSL2 Binding in S2 Cells to genes in S2 Cells that have a 1.2 fold change in response to CLAMP RNAi
perl peaksToGenes.pl --contrast --genome dm3 --name S2_Cells_MSL2_Combined-300-step --contrast_name MSL_ChIP_S2_Cells_contrast_S2_CLAMP_1-2_cutoff --test_genes /users/ahadik/data/jdobson/working_directory/RNA-seq/Analysis/Gene_Lists/parsed-data/s2cells_clamp_paired_rep1_s_2/s2cells_clamp_rnai_paired_rep1_s_2-gene_list_-1-2_fold-change.csv

#Contrast MSL2 Binding in S2 Cells to genes in S2 Cells that have a 2.0 fold change in response to CLAMP RNAi
perl peaksToGenes.pl --contrast --genome dm3 --name S2_Cells_MSL2_Combined-300-step --contrast_name MSL_ChIP_S2_Cells_contrast_S2_CLAMP_2-0_cutoff --test_genes /users/ahadik/data/jdobson/working_directory/RNA-seq/Analysis/Gene_Lists/parsed-data/s2cells_clamp_paired_rep1_s_2/s2cells_clamp_rnai_paired_rep1_s_2-gene_list_-2_fold-change.csv

