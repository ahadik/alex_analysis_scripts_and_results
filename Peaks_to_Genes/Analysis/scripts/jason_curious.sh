#! /bin/bash

#SBATCH -n 2
#SBATCH -t 168:00:00
#SBATCH --mem=MaxMemPerNode
#SBATCH -J 'peakstogenes-contrast'
#SBATCH -o /users/ahadik/scratch/working/peakstogenes-contrast.out
#SBATCH -e /users/ahadik/scratch/working/peakstogenes-contrast.err
#SBATCH --qos='ccmb-condo'
#SBATCH --mail-type=ALL
#SBATCH --mail-user=alexander_hadik@brown.edu

#CD into PeaksToGenes installation to execute script
cd ~/data/jdobson/PeaksToGenes/bin

#load bedtools
module load bedtools

#Define variables
S2_GFP=S2_Cells_GFP-300-step

S2_MSL2=S2_Cells_MSL2-300-step

S2_MSL2_Comb=S2_Cells_MSL2_Combined-300-step

Gene_Directory=/gpfs/data/larschan/jdobson/working_directory/ChIP-seq/alex_clamp_analysis_scripts_and_results/Analysis/PeaksToGenes/07-23-13

#Test gene lists
AUT_test_1_2=autosome_responsive_1_2.csv
AUT_test_2_0=autosome_responsive_2_0.csv
x_test_1_2=xchrom_responsive_1_2.csv
x_test_2_0=xchrom_responsive_2_0.csv

#Background gene lists
AUT_back_1_2=autosome_unresponsive_1_2.csv
AUT_back_2_0=autosome_unresponsive_2_0.csv
x_back_1_2=xchrom_unresponsive_1_2.csv
x_back_2_0=xchrom_unresponsive_2_0.csv

#Calls

#S2 Cells w/ GFP RNAi
	#GENE LIST: S2 CLAMP
		#1.2 Cutoff
			#Autosome
				perl peaksToGenes.pl --contrast --genome dm3 --name $S2_MSL2 --contrast_name MSL2_ChIP_S2_Cells_GFP_RNAi_contrast_S2_MSL2_RNAi_1-2_cutoff_autosome --test_genes ${Gene_Directory}/s2_msl2/test/${AUT_test_1_2} --background_genes ${Gene_Directory}/s2_msl2/background/${AUT_back_1_2}

				perl peaksToGenes.pl --contrast --genome dm3 --name $S2_MSL2 --contrast_name MSL2_ChIP_S2_Cells_GFP_RNAi_contrast_S2_MSL2_RNAi_1-2_cutoff_autosome --test_genes ${Gene_Directory}/s2_msl2/test/${x_test_1_2} --background_genes ${Gene_Directory}/s2_msl2/background/${x_back_1_2}
