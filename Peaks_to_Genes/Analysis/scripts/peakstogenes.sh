#! /bin/bash

#SBATCH -n 2
#SBATCH -t 168:00:00
#SBATCH --mem=MaxMemPerNode
#SBATCH -J 'peakstogenes'
#SBATCH -o /users/ahadik/scratch/working/peakstogenes.out
#SBATCH -e /users/ahadik/scratch/working/peakstogene.err
#SBATCH --qos='ccmb-condo'
#SBATCH --mail-type=ALL
#SBATCH --mail-user=alexander_hadik@brown.edu

cd ~/data/jdobson/PeaksToGenes/bin

module load bedtools

which intersectBed
which mysql
which sqlite3

perl peaksToGenes.pl --signal_ratio --genome dm3 --name Kc_Cells_GFP-300-step --ip_file ~/data/jdobson/working_directory/ChIP-seq/Mapped_Reads/Combined_replicates/Converted_Reads_in_BED_Format/CG1832_ChIP_Kc_Cells_GFP_RNAi_Combined_converted.bed --input_file ~/data/jdobson/working_directory/ChIP-seq/Mapped_Reads/Combined_replicates/Converted_Reads_in_BED_Format/CG1832_Input_Kc_Cells_GFP_RNAi_Combined_converted.bed --processors 8 --scaling 0.945621287967171

perl peaksToGenes.pl --signal_ratio --genome dm3 --name S2_Cells_GFP-300-step --ip_file ~/data/jdobson/working_directory/ChIP-seq/Mapped_Reads/Combined_replicates/Converted_Reads_in_BED_Format/CG1832_ChIP_S2_Cells_GFP_RNAi_Combined_converted.bed --input_file ~/data/jdobson/working_directory/ChIP-seq/Mapped_Reads/Combined_replicates/Converted_Reads_in_BED_Format/CG1832_Input_S2_Cells_GPF_RNAi_Combined_converted.bed --processors 8 --scaling 0.776401539598374

perl peaksToGenes.pl --signal_ratio --genome dm3 --name S2_Cells_MSL2-300-step --ip_file ~/data/jdobson/working_directory/ChIP-seq/Mapped_Reads/Combined_replicates/Converted_Reads_in_BED_Format/CG1832_ChIP_S2_Cells_MSL2_RNAi_Combined_converted.bed --input_file ~/data/jdobson/working_directory/ChIP-seq/Mapped_Reads/Combined_replicates/Converted_Reads_in_BED_Format/CG1832_Input_S2_Cells_MSL2_RNAi_Combined_converted.bed --processors 8 --scaling 0.783691239633501

perl peaksToGenes.pl --signal_ratio --genome dm3 --name S2_Cells_MSL2_Combined-300-step --ip_file ~/data/jdobson/working_directory/ChIP-seq/Mapped_Reads/Combined_replicates/Converted_Reads_in_BED_Format/MSL_ChIP_S2_Cells_Combined_converted.bed --input_file ~/data/jdobson/working_directory/ChIP-seq/Mapped_Reads/Combined_replicates/Converted_Reads_in_BED_Format/MSL_Input_S2_Cells_Combined_converted.bed --processors 8 --scaling 1.08840448781593