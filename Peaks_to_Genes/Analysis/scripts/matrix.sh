#! /bin/bash

#SBATCH -n 2
#SBATCH -t 168:00:00
#SBATCH --mem=MaxMemPerNode
#SBATCH -J 'clamp_peak_matrix'
#SBATCH -o /users/ahadik/scratch/working/clamp_peak_matrix.out
#SBATCH -e /users/ahadik/scratch/working/clamp_peak_matrix.err
#SBATCH --qos='ccmb-condo'
#SBATCH --mail-type=ALL
#SBATCH --mail-user=alexander_hadik@brown.edu

#CD into PeaksToGenes installation to execute script
cd ~/data/jdobson/PeaksToGenes/bin

#load bedtools
module load bedtools

perl peaksToGenes.pl --matrix --genome dm3 --matrix_names S2_Cells_GFP-300-step --gene_list /users/ahadik/data/jdobson/working_directory/ChIP-seq/alex_clamp_analysis_scripts_and_results/Analysis/PeaksToGenes/07-23-13/s2_msl2/background/autosome_unresponsive_2_0.csv