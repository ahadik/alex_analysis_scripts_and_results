#! /bin/bash

cd ~/data/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-09-13

for directory in *
do

	cd $directory

	rm unregulated_autosome_1.2-fold_change.csv
	rm unregulated_autosome_2.0-fold_change.csv
	rm unregulated_xChrom_1.2-fold_change.csv
	rm unregulated_xChrom_2.0-fold_change.csv

	rm regulated_autosome_1.2-fold_change.csv
	rm regulated_autosome_2.0-fold_change.csv
	rm regulated_xChrom_1.2-fold_change.csv
	rm regulated_xChrom_2.0-fold_change.csv

	perl ~/data/jdobson/working_directory/ChIP-seq/alex_clamp_analysis_scripts_and_results/Analysis/scripts/non-reg-gene-extract.pl *data.csv 1.2

	perl ~/data/jdobson/working_directory/ChIP-seq/alex_clamp_analysis_scripts_and_results/Analysis/scripts/non-reg-gene-extract.pl *data.csv 2.0

	cd ..

done
