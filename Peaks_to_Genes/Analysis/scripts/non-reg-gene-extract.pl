#!/usr/bin/perl -w

use strict;
use warnings;
use Data::Dumper;

#Define the number of arguments provided to the script
my $argnum = $#ARGV + 1;

#Check that only one file is entered for parsing
if ($#ARGV != 1)
{
	die "$argnum arguments were received, but 2 arguments are expected";
}

#Define the file path from the provided argument
my $file = $ARGV[0];

print $file . "\n";

#Define the differential expression cutoff provided as an argument

#Define the bound to filter by
my $logBound = $ARGV[1];
my $bound = log($logBound)/log(2);
$bound = -$bound;

#Instantiate an array for holding RefSeq IDs for non-regulated X chromosome genes
my @xChrom;
my @xChromReg;

#Instantiate an array for holding RefSeq IDs for non-regulated autosomal genes
my @autChrom;
my @autChromReg;

#Define a file handle, $fh, to represent the opened file provided as an argument
open (my $fh , "<", $file)
#If the file cannot be opened, die and give a reason.
 or die "Couldn't open $file : $!";

#For each line in the file
while (<$fh>) {
	my $line = $_;
	
	#Remove the new line character
	chomp($line);
	
	#If the line does not begin with test_id
	unless ($line =~ /^test_id/){
		
		#Split the line on commas
		my @line_items = split(/\,/,$line);
		
		#Define the RefSeq ID from the array
		my $refSeq = $line_items[0];
		
		#Define the gene's locus from the array
		my $locus = $line_items[2];
		
		
		#If the gene is on the x chromosome, push to the xChrom array if its log2 fold change is greater than $bound
		if($locus eq "chrX") {

			if($bound < $line_items[5]){
				print $refSeq;	
				push(@xChrom, $refSeq);
			}
			else{
				push(@xChromReg, $refSeq);
			}
			
		}
		
		#If the gene is on the autosome, push to the autChrom array if its log2 fold change is greater than $bound
		if($locus ne "chrX") {

			if($bound < $line_items[5]){
				print $refSeq;
				push(@autChrom, $refSeq);
			}
			else{
				push(@autChromReg, $refSeq);
			}
		}
		
	};
	
}

my $fold_title = $logBound . "-fold_change";

#Open file OUTPUT with name unregulated_xChrom.csv
open (xCHROM, '>>unregulated_xChrom_' . $fold_title . '.csv');
open (xCHROMREG, '>>regulated_xChrom_' . $fold_title . '.csv');

#Open file OUTPUT with name unregulated_autosome.csv
open (AUTO, '>>unregulated_autosome_' . $fold_title . '.csv');
open (AUTOREG, '>>regulated_autosome_' . $fold_title . '.csv');

#for every element of the xChrom array
for (my $i=0; $i < @xChrom; $i++)
{
	print xCHROM ($xChrom[$i] . "\n");
	print  $xChrom[$i] . "\n";
}

#for every element of the xChromReg array
for (my $i=0; $i < @xChromReg; $i++)
{
	print xCHROMREG ($xChromReg[$i] . "\n");
	print  $xChromReg[$i] . "\n";
}

#for every element of the autChrom array
for (my $i=0; $i < @autChrom; $i++)
{
	print AUTO ($autChrom[$i] . "\n");
	print  $autChrom[$i] . "\n";
}

#for every element of the autChromReg array
for (my $i=0; $i < @autChromReg; $i++)
{
	print AUTOREG ($autChromReg[$i] . "\n");
	print  $autChromReg[$i] . "\n";
}

close (AUTO);
close(xCHROM);
close(xCHROMREG);
close(AUTOREG);