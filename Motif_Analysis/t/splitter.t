#!/usr/bin/perl
#
#===============================================================================
#
#         FILE: splitter.t
#
#  DESCRIPTION: This Perl test script is designed to test the functions
#  				of the splitter module which extracts the coordinates of
#				genes on a gene list from a reference BED genome file.
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Alexander Hadik
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 08/07/2013
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use Test::More;
use FindBin;
use lib "$FindBin::Bin/../lib";
use Data::Dumper;
use splitter;

#For comparing arrays
sub arrayCompare 
{
	#Define two input arrays
	
	my($ref_array1, $ref_array2) = @_;
	
	my @array1 = @$ref_array1;
	my @array2 = @$ref_array2;
	
	#Test their equality
	if (@array1 ~~ @array2){return 1;}
	else{return 0;}
}

BEGIN {

	###TEST arrayCompare###
	my @t1 = ("a","b","c");
	my @t2 = ("a","b","c");
	is(arrayCompare(\@t1,\@t2),1,"Test arrayCompare");
	
	#Instantiate splitter class with default values
	
	my @gene_array=("NR_073624", "NM_001258477", "earlyGene", "lateGene","NM_001104042");
	
	my $splitter = splitter->new(genome => 'dm3Test', gene_list => \@gene_array);
	
	
	
	###TEST CHROM SIZE ATTRIBUTE OF SPLITTER###
	can_ok($splitter, 'chrom_sizes');
	isa_ok($splitter->chrom_sizes, 'HASH');
	cmp_ok($splitter->chrom_sizes->{chrX}, '==', 22422827);
	
	###TEST GENE DATA ATTRIBUTE OF SPLITTER###
	can_ok($splitter, 'genome_bed');
	isa_ok($splitter->genome_bed, 'HASH');
	
	
	
	###TEST SUBROUTINE: calcCoords###
	
	#Test that each coordinate of calcCoords is correct for an odd length gene
	is($splitter->data_hash->{tss}[0][1], 1047740, "Odd 5' Upstream");
	is($splitter->data_hash->{tss}[0][2], 1049236, "Odd 5' Downstream");
	is($splitter->data_hash->{tts}[0][1], 1049237, "Odd 3' Upstream");
	is($splitter->data_hash->{tts}[0][2], 1050733, "Odd 3' Downstream");
	
	

	#Test that each coordinate of  calcCoords is correct for an even length gene
	is($splitter->data_hash->{tss}[1][1], 67969, "Even 5' Upstream");
	is($splitter->data_hash->{tss}[1][2], 112587, "Even 5' Downstream");
	is($splitter->data_hash->{tts}[1][1], 112588, "Even 3' Upstream");
	is($splitter->data_hash->{tts}[1][2], 157204, "Even 3' Downstream");

	#Test for zeroing on gene near start of chromosome
	is($splitter->data_hash->{tss}[2][1], 0, "Zero on 5' Upstream");
	is($splitter->data_hash->{tss}[2][2], 67453, "Expected 5' Downstream");
		
	#Test for capping on gene near end of chromosome
	is($splitter->data_hash->{tts}[3][1], 743372, "Expected on 3' Upstream");
	is($splitter->data_hash->{tts}[3][2], 1351857, "Upstream cap on 3' lateGene");
	
	#Test constraint of percentage on distance value
	@gene_array = ("NM_001258477");
	my $perc_splitter = splitter->new(genome => 'dm3Test', gene_list => \@gene_array, distance => 75, percent_distance => 1);
	
	eval {$perc_splitter->calcCoords("NM_001258477") };
	if ($@) {
    	warn "Oh no! [$@]\n";
	}

	#Instantiate splitter class with bp values
	my @gene_list = ("NR_073624");
	my $bp_splitter = splitter->new(genome => 'dm3Test', gene_list => \@gene_list, distance => 1000, bp_distance => 1);
	
	
	
	###TEST SUBROUTINE: calcCoords###

	#Test that each coordinate of calcCoords is correct for an odd length gene
	is($bp_splitter->data_hash->{tss}[0][1], 1047488, "1000 5' Upstream");
	is($bp_splitter->data_hash->{tss}[0][2], 1049488, "1000 5' Downstream");
	is($bp_splitter->data_hash->{tts}[0][1], 1048985, "1000 3' Upstream");
	is($bp_splitter->data_hash->{tts}[0][2], 1050985, "1000 3' Downstream");
	
	@gene_list = ("NR_073624","NM_001014753","NM_001038731","NM_001038770","NM_001042796","NM_001103477","NM_001103546","NM_001144664","NM_001144665");
	
	$bp_splitter = splitter->new(genome=>"dm3", gene_list => \@gene_list, distance => 10, bp_distance => 1);
	is($bp_splitter->data_hash->{tss}[0][2]-$bp_splitter->data_hash->{tss}[0][1], 20, "length check 5'");
	is($bp_splitter->data_hash->{tts}[0][2]-$bp_splitter->data_hash->{tts}[0][1], 20, "length check 3'");
	is($bp_splitter->data_hash->{tss}[1][2]-$bp_splitter->data_hash->{tss}[1][1], 20, "length check 5'");
	is($bp_splitter->data_hash->{tts}[1][2]-$bp_splitter->data_hash->{tts}[1][1], 20, "length check 3'");
	

}
done_testing();