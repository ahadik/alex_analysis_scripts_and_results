#!/usr/bin/perl
#
#===============================================================================
#
#         FILE: GeneSplit.t
#
#  DESCRIPTION: This Perl test script is designed to test the functions
#  				of the GeneSplit module which is the base class for the
#				MotifAnalysis program.
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Alexander Hadik
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 09/13/2013
#     REVISION: ---
#===============================================================================

use strict;
use warnings;

use Test::More;
use FindBin;
use lib "$FindBin::Bin/../lib";
use Data::Dumper;
use dbGetter;

BEGIN {

    use_ok('dbGetter');
	
	my $genome = "dm3";
	
	my $dbGetter = dbGetter->new(genome => $genome);

    can_ok($dbGetter, 'valid_genome');

    can_ok($dbGetter, 'chromosome_sizes');
    isa_ok($dbGetter->chromosome_sizes, 'HASH');
    cmp_ok($dbGetter->chromosome_sizes->{chrX}, '==', 22422827);
    
    
    can_ok($dbGetter, 'genome_data');
    isa_ok($dbGetter->genome_data, 'HASH');
    
    my @test_array = @{$dbGetter->genome_data->{NM_057357}};
    
    cmp_ok($test_array[1], '==', 27406393);
    
    
    
}

done_testing();