#!/bin/bash

cd /Users/ahadik/Oscar/gpfs/scratch/ahadik/downloads/Drosophila_melanogaster/UCSC/dm3/Sequence/Chromosomes

for fasta in *
do

	printf "\n$fasta\n"
	perl /Users/ahadik/Documents/Brown/Larschan\ Lab/alex_analysis_scripts_and_results/Motif_Analysis/t/chromLengthGetter.pl $fasta

done
