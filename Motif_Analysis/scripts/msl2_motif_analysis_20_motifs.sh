#! /bin/bash

#$ -l vlong
#$ -l vf=8G
#$ -cwd
#$ -pe smp 8
#$ -m as


#SBATCH -n 32
#SBATCH --mem=MaxMemPerNode 
#SBATCH -t 168:00:00
#SBATCH -J 'rep_1_analysis_20_motifs'
#SBATCH --qos=normal
#SBATCH -o /users/ahadik/scratch/working/rep_1_analysis_20_motifs.out
#SBATCH -e /users/ahadik/scratch/working/rep_1_analysis_20_motifs.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=alexander_hadik@brown.edu

source /home/ahadik/.bashrc

export PATH="/data/people/ahadik/MEME/bin:/gpfs/main/home/ahadik/bin/x86_64:$PATH"


/data/people/ahadik/perl5/perlbrew//perls/perl-5.16.0/bin/perl bin/GeneSplit.pl --maxw 15 --nmotifs 20 --mod zoops --basepair 250 --np 16 --genome dm3 --gene_list data/s2cells_msl2_rnai_paired_rep1_s_3/chrXdownReg_2.0.txt --genome_2bit static/dm3.2bit --job_name msl2_rnai_paired_XReg_nmotifs_20 --maxsize 1000000000 --output_dir data/s2cells_msl2_rnai_paired_rep1_s_3/ --fa_output
