#! /bin/bash

#$ -l vlong
#$ -l vf=8G
#$ -cwd
#$ -pe smp 8
#$ -m as


#SBATCH -n 32
#SBATCH --mem=MaxMemPerNode 
#SBATCH -t 168:00:00
#SBATCH -J 'rep_1_analysis'
#SBATCH --qos=normal
#SBATCH -o /users/ahadik/scratch/working/rep_1_analysis.out
#SBATCH -e /users/ahadik/scratch/working/rep_1_analysis.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=alexander_hadik@brown.edu

source /home/ahadik/.bashrc

export PATH="/data/people/ahadik/MEME/bin:/gpfs/main/home/ahadik/bin/x86_64:$PATH"

PIDS=()

/data/people/ahadik/perl5/perlbrew//perls/perl-5.16.0/bin/perl bin/GeneSplit.pl --basepair 250 --np 16 --genome dm3 --gene_list data/s2cells_msl2_rnai_unpaired_rep1_s_8/chrXdownReg_-foldchange.txt --genome_2bit static/dm3.2bit --job_name msl2_rnai_unpaired_XReg --maxsize 1000000000 --output_dir data/s2cells_msl2_rnai_unpaired_rep1_s_8/ --fa_output

PIDS+=($@)

/data/people/ahadik/perl5/perlbrew//perls/perl-5.16.0/bin/perl bin/GeneSplit.pl --basepair 250 --np 16 --genome dm3 --gene_list data/s2cells_msl2_rnai_paired-unpaired/chrXdownReg_-foldchange.txt --genome_2bit static/dm3.2bit --job_name msl2_rnai_paired_unpaired_XReg --maxsize 1000000000 --output_dir data/s2cells_msl2_rnai_paired-unpaired/ --fa_output

PIDS+=($@)

/data/people/ahadik/perl5/perlbrew//perls/perl-5.16.0/bin/perl bin/GeneSplit.pl --basepair 250 --np 16 --genome dm3 --gene_list data/s2cells_msl2_rnai_paired_rep1_s_3/chrXdownReg_-foldchange.txt --genome_2bit static/dm3.2bit --job_name msl2_rnai_paired_XReg --maxsize 1000000000 --output_dir data/s2cells_msl2_rnai_paired_rep1_s_3/ --fa_output

PIDS+=($@)

wait ${PIDS[@]}
