#! /bin/bash

#$ -l vlong
#$ -l vf=8G
#$ -cwd
#$ -pe smp 8
#$ -m as


#SBATCH -n 32
#SBATCH --mem=MaxMemPerNode 
#SBATCH -t 168:00:00
#SBATCH -J 'rep_1_analysis'
#SBATCH --qos=normal
#SBATCH -o /users/ahadik/scratch/working/rep_1_analysis.out
#SBATCH -e /users/ahadik/scratch/working/rep_1_analysis.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=alexander_hadik@brown.edu

source /home/ahadik/.bashrc

export PATH="/data/people/ahadik/MEME/bin:/gpfs/main/home/ahadik/bin/x86_64:$PATH"

cd /data/people/ahadik/alex_analysis_scripts_and_results/Motif_Analysis

/data/people/ahadik/perl5/perlbrew//perls/perl-5.16.0/bin/perl bin/GeneSplit.pl --basepair 250 --np 8 --maxw 15 --genome mm9 --nmotifs 10 --gene_list data/test/gene_lists/RUNX_up.txt --genome_2bit static/mm9.2bit --job_name RUNX_down_reg_control_test --maxsize 1000000000 --output_dir data/test/control_genes/ --fa_output

/data/people/ahadik/perl5/perlbrew//perls/perl-5.16.0/bin/perl bin/GeneSplit.pl --basepair 250 --np 8 --maxw 15 --genome mm9 --nmotifs 10 --gene_list data/test/gene_lists/RUNX_down.txt --genome_2bit static/mm9.2bit --job_name RUNX_up_reg_control_test --maxsize 1000000000 --output_dir data/test/control_genes/ --fa_output
