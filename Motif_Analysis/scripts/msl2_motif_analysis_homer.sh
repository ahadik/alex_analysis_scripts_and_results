#! /bin/bash

#$ -l vlong
#$ -l vf=8G
#$ -cwd
#$ -pe smp 8
#$ -m as

source /home/ahadik/.bashrc

export PATH="/data/people/ahadik:/data/people/ahadik/weblogo:/data/people/ahadik/MEME/bin:/gpfs/main/home/ahadik/bin/x86_64:$PATH"

export PERLBREW_ROOT="/data/people/ahadik/perl5/perlbrew/"

source /data/people/ahadik/perl5/perlbrew//etc/bashrc

findMotifs.pl data/s2cells_msl2_rnai_paired_rep1_s_3/gene_lists/chrXdownReg_2.0.txt dm3 data/s2cells_msl2_rnai_paired_rep1_s_3/homer_results/ -start -250 -end 250 -len 15
