package filter;

public class MissingArgException extends Exception {
	public MissingArgException(String message){
		super(message);
	}
}
