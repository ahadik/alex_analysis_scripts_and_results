package filter;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class ListWriter {

String dirOut;
	
	public void write(String fileName, ArrayList<String> genes) throws FileNotFoundException, UnsupportedEncodingException{
		//Define a new PrintWriter with the file path passed as an argument
				PrintWriter writer = new PrintWriter(dirOut+"/"+fileName, "UTF-8");
				
				//For every gene in the provided gene list, print it to the file
				for (String gene : genes){
					writer.println(gene);
				}
				//Close the writer
				writer.close();
	}
	
	//Define the constructor for the ListWriter
	public ListWriter(String dirOut){
		this.dirOut = dirOut;
	}
	
	
}
