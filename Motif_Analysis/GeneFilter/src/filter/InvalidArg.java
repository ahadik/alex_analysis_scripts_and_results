package filter;

public class InvalidArg extends Exception{
	public InvalidArg(String message) {
		super(message);
	}
}
