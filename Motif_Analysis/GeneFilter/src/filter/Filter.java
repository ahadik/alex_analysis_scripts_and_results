package filter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import filter.InvalidArg;
import filter.MissingArgException;
import filter.ListWriter;
import filter.DiffParser;

public class Filter {

	//Instantiate a new entries ArrayList
	ArrayList<String[]> entries = new ArrayList<String[]>();
	Double foldChange;
	
	HashMap<String, Boolean> validChroms = new HashMap<String, Boolean>();
	
	//Chromosome names, hashed to a HashMap of upregulated, vs. downregulated genes, each with an ArrayList of corresponding Genes.
	HashMap<String, HashMap<String, ArrayList<String>>> GeneLists = new HashMap<String, HashMap<String, ArrayList<String>>>();
	
	public Filter(DiffParser parser, Double foldChange){
		//Set the ArrayList entries and fold change from the provided parser object and foldChange double
		this.entries = parser.entries;
		//Convert fold change into log2(foldchange)
		this.foldChange = Math.log(foldChange)/Math.log(2.0);
	}
	
	//Enter all entries of the chroms String[] into the validChroms HashMap
	public void fillValidChroms(String[] chroms){
		for(int i=0; i<chroms.length;i++){
			validChroms.put(chroms[i], true);
		}
	}
	
	public void setGeneMap(String chrom){
		if(GeneLists.get(chrom)==null){
			HashMap<String, ArrayList<String>> genes = new HashMap<String, ArrayList<String>>();
			ArrayList<String> up = new ArrayList<String>();
			ArrayList<String> down = new ArrayList<String>();
			ArrayList<String> non = new ArrayList<String>();
			genes.put("up", up);
			genes.put("down", down);
			genes.put("non", non);
			GeneLists.put(chrom, genes);
		}
	}
	
	public Boolean checkChrom(String chrom){
		Boolean check = validChroms.get(chrom);
		if (check == null){
			return false;
		}else{
			return true;
		}
	}
	
	//Define a function that updates the global ArrayList variables storing the genes
	public void filterGenes(String[] chroms){
		//Fill the validChroms HashMap
		fillValidChroms(chroms);		
		//Define a value for infinity
		double inf = Double.POSITIVE_INFINITY;
		//Set a Boolean to keep track if this is the first line in the file
		boolean First = true;
		//For every entry in the entries ArrayList from the DiffParser
		for (String[] entry : entries){
			//Split the 4th entry to get the chromosome name
			String[] chromSplit = entry[3].split(":");
			String chrom = chromSplit[0];
			
			//If this isn't the first line
			if (!First){
				//If this gene is on a valid chromosome
				if(checkChrom(chrom)){
					//Check the corner case that the value is -inf
					if(entry[9].equals("-inf")){
						setGeneMap(chrom);
						//Add the gene of the current entry to the corresponding chromosome entry in the down regulated ArrayList
						GeneLists.get(chrom).get("down").add(entry[0]);
					//If the value isn't -inf
					}else{
						//If the value isn't inf - then it is a parseable double
						if (!entry[9].equals("inf")){
							//If the double parses to a value less than the cap provided by the user
							if (Double.parseDouble(entry[9]) <= -this.foldChange){
								setGeneMap(chrom);
								GeneLists.get(chrom).get("down").add(entry[0]);
							
							//If the value is greater than the provided cap, the gene is not responsive, or had increased expression
							}else{
								if(Double.parseDouble(entry[9]) >= this.foldChange){
									setGeneMap(chrom);
									GeneLists.get(chrom).get("up").add(entry[0]);
								}else{
									setGeneMap(chrom);
									GeneLists.get(chrom).get("non").add(entry[0]);
								}
							}
						//Else, this gene is up-regulated inf and is added to the up-regulated list
						}else{
							setGeneMap(chrom);
							GeneLists.get(chrom).get("up").add(entry[0]);
						}
					}
				}
			}
			if (First){
				First = false;
			}
		}
	}
	
	public void printGenes(String dirOut, String cap) throws FileNotFoundException, UnsupportedEncodingException{
		
		String fileName = cap+".txt";
		
		//Instantiate a new list writer
		ListWriter writer = new ListWriter(dirOut);
		
		Iterator<Entry<String, HashMap<String, ArrayList<String>>>> it = GeneLists.entrySet().iterator();
		
		while (it.hasNext()){
			@SuppressWarnings("rawtypes")
			Map.Entry chromEntry = (Map.Entry)it.next();
			String chrom = (String) chromEntry.getKey();
			
			@SuppressWarnings("unchecked")
			HashMap<String, ArrayList<String>> chromDivisions = (HashMap<String, ArrayList<String>>) chromEntry.getValue();
			ArrayList<String> up = chromDivisions.get("up");
			ArrayList<String> down = chromDivisions.get("down");
			ArrayList<String> non = chromDivisions.get("non");
			
			writer.write(chrom+"upReg_"+fileName, up);
			writer.write(chrom+"downReg_"+fileName, down);
			writer.write(chrom+"nonReg_"+fileName, non);
		}
	}
	
	public enum Arg
	{
		foldchange, chroms, dirOut, h
	}
	
	public static void main(String[] args) throws IOException, InvalidArg, MissingArgException{
		//Define an argument for the file path provided by the command line argument
		String filepath = args[0];
		
		String help = "This program takes four command line arguments all of which are required. The first argument is "
				+ "the path to a .diff file of isoform expression, which is the output of the CuffDiff algorithm. "
				+ "Three of these arguments are called in any order, indicated by flags:\n\n"
				+ "-foldchange\n"
				+ "-chroms\n"
				+ "-dirOut\n\n"
				+ "The -foldchange flag is to be followed by a number (double) indicating the desired log(2) foldchange "
				+ "to be filtered for. The program will output text lists of all genes that have been either upregulated "
				+ "or downregulated by an amount greater than this value.\n"
				+ "The -chroms flag is to be followed by a comma separated list (no spaces!) of chromosomes to be filtered "
				+ "by the program. A separate text file for each chromosome will be output containing the genes on that chromosome "
				+ "found in the provided .diff file that have been sufficiently regulated, or non regulated.\n"
				+ "The -dirOut flag is to be followed by a path, relative to the current working directory, where the output files "
				+ "will be stored.\n\n"
				+ "If the above arguments are properly provided, three text files for each chromosome will be created. One will contain "
				+ "the genes on that chromosome, found in the .diff file, that have been upregulated, one for the downregulated, and one "
				+ "for the genes that were not sufficiently regulated.";
		
		//Catch the -h flag entered as a first argument, instead of a path to a .diff file
		if(filepath.equals("-h") || filepath.equals("-help")){
			System.out.println(help);
		}
		
		//Define an argument for the fold change cap
		Double foldChange = null;
		//Define a variable for the output directory
		String dirOut = null;
		String[] chroms = null;
		
		for (int i=1; i<args.length; i++){
			String arg = args[i];
			//If the command line argument begins with a dash, it's a flag
			if (arg.substring(0,1).equals("-")){
				//Pattern match on the flags to set the necessary arguments provided by the user.
				switch(Arg.valueOf(arg.substring(1)))
				{
				case foldchange:
					
					//Increment the i index and parse the following argument to a double
					i++;
					try{
						foldChange = Double.parseDouble(args[i]);
					} catch (NumberFormatException e) {
						System.err.println("NumberFormatException: " + e.getMessage());
					}
					break;
				case chroms:
					
					i++;
					String chromList = args[i];
					chroms = chromList.split(",");
					break;
				case dirOut:
					i++;
					dirOut = args[i];
					break;
				case h:
					
					System.out.println(help);
					break;
				default:
					
					throw new InvalidArg("Invalid Argument: "+args[i]);
				}
			}
		}
		
		if ((foldChange == null) || (dirOut == null) || (chroms == null))
		{
			throw new MissingArgException("Missing Arguments! You must provide the following command line arguments:\n\n-foldChange\n-outDir\n-chroms\n\nUse the -h flag for more detailed assistance.");
		}
		
		//Create a new instance of the DiffParser
		DiffParser parser = new DiffParser(filepath);
		
		//Instantiate a new instance of the Filter class
		Filter geneFilterer = new Filter(parser,foldChange);
		//Call the filterGenes function to fill the ArrayList with gene names for sufficiently regulated genes
		geneFilterer.filterGenes(chroms);
		//Call the printGenes function to return the filtered genes to Standard out
		geneFilterer.printGenes(dirOut, foldChange.toString());
		
	}
	
}
