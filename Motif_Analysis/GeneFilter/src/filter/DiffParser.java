package filter;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class DiffParser {

	//Instantiate a String to hold the path to the file of reads
	String filePath;

	//Instantiate a String to hold the read
	ArrayList<String[]> entries = new ArrayList<String[]>();
	
	//Create a constructor for ReadParser class that sets the value of filePath
	DiffParser(String filepath) throws IOException{
		//Set the value of filePath
		this.filePath = filepath;
		
		//Define a FileReader object from the filePath
		FileReader input = new FileReader(this.filePath);
		BufferedReader bufRead = new BufferedReader(input);
		
		//Instantiate a read variable to hold the values of the file during iteration
		String read;
		String seq = null;
			//For all lines in the text file
			while (( read = bufRead.readLine()) != null)
			{
				entries.add(read.split("\t"));
			}
	}
	
}
