#! /bin/bash

#SBATCH -n 32
#SBATCH --mem=MaxMemPerNode 
#SBATCH -t 168:00:00
#SBATCH -J 'rep_1_analysis'
#SBATCH --qos=normal
#SBATCH -o /users/ahadik/scratch/working/rep_1_analysis.out
#SBATCH -e /users/ahadik/scratch/working/rep_1_analysis.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=alexander_hadik@brown.edu

module load twoBitToFa

module load meme


perl bin/GeneSplit.pl --percent .5 --np 32 --genome dm3 --gene_list data/kccells_clamp_rnai_paired_rep1_s_8/XReg_2.0.txt --genome_2bit static/dm3.2bit --job_name clamp_rnai_XReg_2.0 --maxsize 100000000 --output_dir data/kccells_clamp_rnai_paired_rep1_s_8/


perl bin/GeneSplit.pl --percent .5 --np 32 --genome dm3 --gene_list data/kccells_gfp_rnai_paired_rep1_s_6/XReg_2.0.txt --genome_2bit static/dm3.2bit --job_name gfp_rnai_XReg_2.0 --maxsize 100000000 --output_dir data/kccells_gfp_rnai_paired_rep1_s_6/




perl bin/GeneSplit.pl --percent .5 --np 32 --genome dm3 --gene_list data/s2cells_clamp_and_msl2_rnai_paired_rep1_s_5/XReg_2.0.txt --genome_2bit static/dm3.2bit --job_name clamp_and_msl2_rnai_XReg_2.0 --maxsize 100000000 --output_dir data/s2cells_clamp_and_msl2_rnai_paired_rep1_s_5/



perl bin/GeneSplit.pl --percent .5 --np 32 --genome dm3 --gene_list data/s2cells_clamp_rnai_paired_rep1_s_2/XReg_2.0.txt --genome_2bit static/dm3.2bit --job_name clamp_rnai_XReg_2.0 --maxsize 100000000 --output_dir data/s2cells_clamp_rnai_paired_rep1_s_2/



perl bin/GeneSplit.pl --percent .5 --np 32 --genome dm3 --gene_list data/s2cells_gfp_rnai_paired_rep1_s_1/XReg_2.0.txt --genome_2bit static/dm3.2bit --job_name gfp_rnai_XReg_2.0 --maxsize 100000000 --output_dir data/s2cells_gfp_rnai_paired_rep1_s_1/




perl bin/GeneSplit.pl --percent .5 --np 32 --genome dm3 --gene_list data/s2cells_msl2_rnai_paired_rep1_s_3/XReg_2.0.txt --genome_2bit static/dm3.2bit --job_name msl2_rnai_XReg_2.0 --maxsize 100000000 --output_dir data/s2cells_msl2_rnai_paired_rep1_s_3/

