use utf8;
package UCSC;

use Moose;
extends 'DBIx::Class::Schema';

__PACKAGE__->load_namespaces;

=head1 NAME

UCSC - Genome Data

=head1 AUTHOR

Jason R Dobson, L<foxprimer@gmail.com>

=head1 LICENSE

This library is free software. You can redistribute it and/or modify it under
the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
