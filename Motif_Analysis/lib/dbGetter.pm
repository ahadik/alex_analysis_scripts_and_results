package dbGetter;
use Moose;
use FindBin;
use lib "$FindBin::Bin/../lib";
use autodie;
use Data::Dumper;
use UCSC;

=head1 NAME

dbGetter

=head1 DESCRIPTION

This module is used to determine which genomes are available for download from the UCSC MySQL
database, and then download the chromosome sizes file, and refSeq data.

=cut

=head1 AUTHOR

Alexander H Hadik L<alexander_hadik@brown.edu>

=head1 LICENSE

This library is free software. You can redistribute it and/or modify it under
the same terms as Perl itself.

=cut

=head2 genome

This Moose object contains the string indicating the user set genome

=cut
has genome => (
    is => 'ro',
    isa => 'Str',
    required => 1,
    default => sub{
        croak("\n\nYou must define the desired genome\n\n")
    }
);

=head2 ucsc_genomes

This Moose object contains a Hash Ref of available genomes from UCSC.

=cut
has ucsc_genomes    =>  (
    is          =>  'ro',
    isa         =>  'HashRef',
    predicate   =>  'has_ucsc_genomes',
    writer      =>  '_set_ucsc_genomes',
);

#Before: logical check - checks that all necessary attributes have been set, and if they have not, set each of them by calling their writers and constructors
before 'ucsc_genomes' => sub {
    my $self = shift;
    #Do logical check to see if attributes need to be set
    unless ( $self->has_ucsc_genomes ) {
        $self->_set_ucsc_genomes($self->_define_ucsc_genomes);
    }
};

=head2 _define_ucsc_genoems

This private subroutine is dynamically called to populate the Hash Ref of
genomes available from UCSC.

=cut

sub _define_ucsc_genomes    {
    my $self = shift;

    # Define a Hash Ref of valid genome strings
    my $genome_info = {
        hg19    =>  1,
        hg18    =>  1,
        hg17    =>  1,
        hg16    =>  1,
        panTro3 =>  1,
        panTro2 =>  1,
        panTro1 =>  1,
        ponAbe2 =>  1,
        rheMac2 =>  1,
        calJac3 =>  1,
        calJac1 =>  1,
        mm10    =>  1,
        mm9 =>  1,
        mm8 =>  1,
        mm7 =>  1,
        rn5 =>  1,
        rn4 =>  1,
        rn3 =>  1,
        cavPor3 =>  1,
        oryCun2 =>  1,
        oviAri1 =>  1,
        bosTau7 =>  1,
        bosTau6 =>  1,
        bosTau4 =>  1,
        bosTau3 =>  1,
        bosTau2 =>  1,
        equCab2 =>  1,
        equCab1 =>  1,
        felCat4 =>  1,
        felCat3 =>  1,
        canFam3 =>  1,
        canFam2 =>  1,
        canFam1 =>  1,
        monDom5 =>  1,
        monDom4 =>  1,
        monDom1 =>  1,
        ornAna1 =>  1,
        galGal4 =>  1,
        galGal3 =>  1,
        galGal2 =>  1,
        taeGut1 =>  1,
        xenTro3 =>  1,
        xenTro2 =>  1,
        xenTro1 =>  1,
        danRer7 =>  1,
        danRer6 =>  1,
        danRer5 =>  1,
        danRer4 =>  1,
        danRer3 =>  1,
        fr3 =>  1,
        fr2 =>  1,
        fr1 =>  1,
        gasAcu1 =>  1,
        oryLat2 =>  1,
        dm3 =>  1,
        dm2 =>  1,
        dm1 =>  1,
        ce10    =>  1,
        ce6 =>  1,
        ce4 =>  1,
        ce2 =>  1,
        ce10    =>  1,
        #Add entry to allow for testing w/ custom files
        dm3Test    =>  1,
    };
    return $genome_info;
}

=head2 ucsc_genomes

This Moose object contains a schema for the UCSC MySQL database.

=cut

has ucsc_schema =>  (
    is          =>  'ro',
    isa         =>  'UCSC',
    predicate   =>  'has_ucsc_schema',
    writer      =>  '_set_ucsc_schema',
);

#Before: logical check - checks that all necessary attributes have been set, and if they have not, set each of them by calling their writers and constructors
before 'ucsc_schema' => sub {
    my $self = shift;
    #Do logical check to see if attributes need to be set
    unless ( $self->has_ucsc_schema ) {
        $self->_set_ucsc_schema($self->_get_ucsc_schema);
    }
};

=head2 _get_ucsc_schema

This private subroutine is called dynamically to get the UCSC::Schema object for
the user-defined genome.

=cut

sub _get_ucsc_schema    {
    my $self = shift;
    my $ucsc_schema = $self->define_ucsc_schema($self->genome);
    return $ucsc_schema;
}



=head2 define_ucsc_schema

This subroutine is passed a genome string and returns a DBIx::Class::Schema
object with a connection to the UCSC MySQL server.

=cut

sub define_ucsc_schema {
    my $self = shift;
    my $genome = shift;
    
    # Connect to the UCSC MySQL Browser
    my $schema =
    UCSC->connect('dbi:mysql:host=genome-mysql.cse.ucsc.edu;database='
        . $genome, "genome");
    return $schema;
}

=head2 chromosome_sizes

This Moose attribute is dynamically defined based on the user-defined genome and
contains a Hash Ref of chromosome names as keys and integer lengths as values.

=cut

has chromosome_sizes    =>  (
    is          =>  'ro',
    isa         =>  'HashRef',
    predicate   =>  'has_chromosome_sizes',
    writer      =>  '_set_chromosome_sizes',
);

#Before referencing chromosome sizes, check if it has been set. If it has not, call its constructor.
before 'chromosome_sizes' => sub {
    my $self = shift;
    #Do logical check to see if attributes need to be set
    unless ( $self->has_chromosome_sizes ) {
        $self->_set_chromosome_sizes($self->_get_chromosome_sizes);
    }
};


=head2 _get_chromosome_sizes

This private subroutine is run dynamically to fetch the chromosome sizes from
the UCSC MySQL server.

=cut

sub _get_chromosome_sizes   {
    my $self = shift;

    # Pre-declare Hash Ref
    my $chrom_sizes = {};

    # Define the path to the chromosome sizes file
    my $chrom_size_path = "$FindBin::Bin/../static/".$self->valid_genome.".sizes";
    
    #Check if the chromosome files size already exists
    if ( ! -s $chrom_size_path ){
        # Get the chromosome sizes from UCSC and store them in a HashRef
        my $raw_chrom_sizes = $self->ucsc_schema->storage->dbh_do(
            sub {
                my ($storage, $dbh, @args) = @_;
                $dbh->selectall_hashref("SELECT chrom, size FROM chromInfo",
                    ["chrom"]);
            },
        );

		#create a file in the assumed path for this genome's chromsome sizes and open it for writing 
        open my $chrom_file_out, ">>", $chrom_size_path;
        # Parse the chromosome sizes file into an easier to use form
        foreach my $chromosome (keys %{$raw_chrom_sizes}) {
            $chrom_sizes->{$chromosome} = $raw_chrom_sizes->{$chromosome}{size};
            print $chrom_file_out join("\t", $chromosome,
                $raw_chrom_sizes->{$chromosome}{size}
            ), "\n";
        }
        close $chrom_file_out;
        return $chrom_sizes;
    } else {
        # Want to open the file and return a Hash Ref of data parsed from the file
        open my $chr_sizes_file, "<", $chrom_size_path;
        while(<$chr_sizes_file> ) {
            my $line = $_;
            chomp($line);
            my ($chr, $length) = split(/\t/, $line);
            $chrom_sizes->{$chr} = $length;
        }
        close $chr_sizes_file;
        return $chrom_sizes;
    }
}

#valid_genome: an attribute for the genome that is set only after determining if the genome matches an entry in the UCSC database
has valid_genome => (
    is => 'ro',
    isa => 'Str',
    predicate => 'has_valid_genome',
    writer => 'set_valid_genome'
);

#before valid_genome is set, check that the value set for the genome attribute is an entry in the UCSC database 
before 'valid_genome' => sub {
    my $self = shift;
    unless ( $self->has_valid_genome ) {
        if  (exists $self->ucsc_genomes->{$self->genome}) {
            $self->set_valid_genome($self->genome);
        }else{die "Invalid genome entered";}
    }
};

=head2 genome_data

This Moose attribute is dynamically defined based on the user-defined genome and
contains a Hash Ref of gene RefSeqs as keys and arrays of gene data as values.

=cut

has genome_data    =>  (
    is          =>  'ro',
    isa         =>  'HashRef',
    predicate   =>  'has_genome_data',
    writer      =>  '_set_genome_data',
);

#Before: logical check - checks that all necessary attributes have been set, and if they have not, set each of them by calling their writers and constructors
before 'genome_data' => sub {
    my $self = shift;
    #Do logical check to see if attributes need to be set
    unless ( $self->has_genome_data ) {
        $self->_set_genome_data($self->_get_genome_data);
    }
};

=head2 _get_genome_data

This private subroutine is run dynamically to fetch the gene data for a given genome from
the UCSC MySQL server and return it in the form of a HashRef.

=cut

sub _get_genome_data {
	my $self = shift;
	
	my $gene_data = {};
	
	my $gene_data_path = "$FindBin::Bin/../static/".$self->valid_genome.".bed";
	
	if (! -s $gene_data_path ){
		my $raw_gene_data = $self->ucsc_schema->storage->dbh_do(
			sub {
				my ($bed_storage, $bed_dbh, @bed_args) = @_;
				$bed_dbh->selectall_hashref("SELECT name, chrom, strand, txStart, txEnd FROM refGene",
                ["name"]);
        		},
    		);
    		
    	# Parse the chromosome sizes file into an easier to use form and print it to a file
    	open my $gene_data_out, ">>", $gene_data_path;
    	#Loop through all elements of the retrieved HashRef
   		foreach my $refName (keys %$raw_gene_data) {
   			my @gene_data_array = ($raw_gene_data->{$refName}{chrom}, $raw_gene_data->{$refName}{txStart}, $raw_gene_data->{$refName}{txEnd}, $raw_gene_data->{$refName}{name}, 0, $raw_gene_data->{$refName}{strand});

        	$gene_data->{$refName} = \@gene_data_array;
   			print $gene_data_out join("\t", @{$gene_data->{$refName}}), "\n";
   		}
   		close $gene_data_out;
   		return $gene_data;
   	#Otherwise, the file does exist and can be read locally
	}else{
		open my $gene_data_file,  "<", $gene_data_path;
		while (<$gene_data_file>) {
			my $line = $_;
			chomp $line;
			my @gene_data_array = split(/\t/,$line);
			
			$gene_data->{$gene_data_array[3]} = \@gene_data_array;
		}
		close $gene_data_file;
		return $gene_data;
	}
}
1;