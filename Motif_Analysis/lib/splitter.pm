package splitter 0.001;
use Moose;
use FindBin;
use lib "$FindBin::Bin/../lib";
use Data::Dumper;
use POSIX;
use Carp;
use Moose::Util::TypeConstraints;
use dbGetter;

=head1 NAME

splitter

=cut

=head1 AUTHOR

Alexander Hadik, <alexander_hadik@brown.edu>

=cut

=head1 DESCRIPTION

The splitter module takes a list of genes, and their coordinates on chromosomes, and produces 
a dataset that lists four coordinates derived from the original data:

*define n to be 1/2 the total length of gene m*

UpStreamStart : the coordinate n basepairs upstream of m tss
DownStreamStart : the coordinate n basepairs downstream of m tss
UpStreamEnd : n basepairs upstream of m tts
DownStreamEnd : n basepairs downstream of m tts

Two sets of supporting data are necessary:
	-*.chrom.sizes : A tab delimited file that describes the length of each chromosome for the desired genome
	-*.bed : A tab delimited BED file for the desired genome that lists the chromosome, start coord and end coord for as many genes are desired
	-Multiples of each of these files may be provided. The user will be prompted to choose the desired file upon execution. 

=cut

=head1 INSTALLATION

A single directory must be configured for each desired genome. Genome directories are stored in the static directory and their names are the arguments passed to splitter when used.
For each genome directory in the static directory, a *.chrom.sizes file and a *.bed file must be provided.

=cut

=head1 SYNOPSIS

Module Use:

use splitter;

my $desired_genome = "NAME/OF/DESIRED/GENOME/DIRECTORY/WITHIN/STATIC";

my $splitter = splitter->new(
    genome     =>  $desired_genome
);

Instantiate an instance of the module as described above, declaring the desired genome directory upon instantiation. This value is used to construct the necessary paths for the chromosome sizes file, and the BED file. 

=cut

=head1 AVAILABLE ATTRIBUTES

Upon instantiation, a hash is created with key/value pairs of refSeq IDs and their associated data from the bed file.

Drawing from whatever source is desired, query this hash with refSeq IDs, and pass the returned array to the calcCoords function to calculate the four coordinates described above in DESCRIPTION

Access the calcCoord function using the command:

my $refSeqID = "desired refSeq ID"'
my $geneDataHashRef = splitter->genome_bed;
my %geneDataHash = %{$geneDataHashRef};

my $singleGeneDataArrayRef = $geneDataHash{$refSeqID};

my @coordData = splitter::calcCoords($singleGeneDataArrayRef);

Note that an array reference from the gene data hash is passed as an argument to calcCoords.

=cut

=head2 genome

A string matching a genome in the USCS database set by the user representing the desired genome.

=cut

has genome => (
	is => 'ro',
	isa => 'Str',
	required => 1,
	default => sub{
		croak("\n\nYou must define the desired genome\n\n")
	}
);

=head2 valid_distance

This subtype ensures that any percent distace value must be less than or equal to 50.

=cut

subtype 'valid_distance', as 'Num', where {$_ <= .5 && $_ > 0},
message {"$_ is not a valid distance. Valid distance must be greater than 0 and less than .5"};

=head2 distance

A number representing the desired distance from each end of a gene, defined as either a number of basepairs, or a percentage of the gene.

Constraints
	-A percentage can be no more than .5
	-A bp distance will be reduced to at most half the length of a gene if it exceeds half the length of the gene

=cut

has distance => (
	is => 'ro',
	isa => 'Num',
	predicate => 'has_distance_value',
	writer => '_set_distance_value'
);

#Before referencing this attribute, see if it was set during instantiation of the class. If it wasn't, call the _get_distance_value function to set it automatically
before 'distance' => sub {
	my $self = shift;
	unless ( $self->has_distance_value ) {
		$self->_set_distance_value ($self->_get_distance_value);
	}
	unless ($self->has_percent_distance){
		$self->_set_percent_distance(1);
	}
};

has valid_percent => (
	is => 'ro',
	isa => 'valid_distance',
	predicate => 'has_percent_value',
	writer => '_set_percent_value'
);

#When the attribute valid_percent is referenced, set its value as type valid_distance from the user defined distance value
before 'valid_percent' => sub {
	my $self = shift;
	unless ($self->has_percent_value) {
		$self->_set_percent_value ($self->distance)
	}
};

has valid_bp => (
	is => 'ro',
	isa => 'Int',
	predicate => 'has_bp_value',
	writer => '_set_bp_value'
);

before 'valid_bp' => sub {
	my $self = shift;
	unless ($self->has_bp_value) {
		$self->_set_bp_value ($self->distance)
	}
};

=head2 _get_distance_value

Automatically define the value for the distance if the user did not set it upon instantiation

=cut

sub _get_distance_value {
	my $self = shift;

	#Set two default values to be used for percentage and bp when the user provides no preference themselves.
	my $default_distance_bp = 50;
	my $default_distance_perc = 0.5;
	
	if ($self->percent_distance){
		return $default_distance_perc;
	}
	if ($self->bp_distance){
		return $default_distance_bp;
	}
}

=head2 percent_distance

An integer representing a boolean for what unit distance is represented by for calculating coordinates. 
A false, or non-positive value, indicates the distance value represented base pairs. A positive number
indicates the distance value represents a percentage of the gene body's length.

=cut

has percent_distance => (
	is => 'ro',
	isa => 'Bool',
	predicate => 'has_percent_distance',
	writer => '_set_percent_distance',
);

#Check if a percent distance boolean was set by the user. If it wasn't default to true.
before 'percent_distance' => sub {
	my $self = shift;
	unless ($self->has_percent_distance) {
		#Default to 0, and then set to 1 by default if both percent_distance and bp_distance are set to 0
			$self->_set_percent_distance(0);
	}
};

has bp_distance => (
	is => 'ro',
	isa => 'Bool',
	predicate => 'has_bp_distance',
	writer =>'_set_bp_distance',
);

#If the bp-distance boolean hasn't been set by the user, default to 0. This will be dynamically set to 0 by
#the dynamic setting of percent_distance.
before 'bp_distance' => sub {
	my $self = shift;
	unless ($self->has_bp_distance) {
		$self->_set_bp_distance(0);
	}
};

=head2 dbGetter

An intance of dbGetter, instantiated upon instantiation of splitter module, to ensure that necessary bed and chrom.sizes files are available for reference

=cut

has dbGetter => (
	is => 'ro',
	isa => 'dbGetter',
	predicate => 'has_dbGetter',
	writer => '_instantiate_dbGetter'
);

before 'dbGetter' => sub {
	my $self = shift;
	unless ( $self->has_dbGetter ) {
		$self->_instantiate_dbGetter (dbGetter->new(genome => $self->genome));
	}
};

=head2 genome_bed

A HashRef that stores the parsed BED file defined by the user's desired genome

=cut

has genome_bed => (
	is => 'ro',
	isa => 'HashRef',
	predicate => 'has_genome_bed',
	writer => '_set_genome_bed'
);

before 'genome_bed' => sub {
	my $self = shift;
	unless ( $self->has_genome_bed ) {
		$self->_set_genome_bed ($self->dbGetter->genome_data);
	}
};


=head2 chrom_sizes

-HashRef attribute of object that instantiates a Hash to hold the sizes for each chromosome

=cut

has chrom_sizes => (
	is => 'ro',
	isa => 'HashRef',
	predicate => 'has_chrom_sizes',
	writer => '_set_chrom_sizes'
);

=head1

Before referencing chromosome sizes, see if it's set. If it's not, set it by referencing the necessary attributes of dbGetter.

=cut

before 'chrom_sizes' => sub {
	my $self = shift;
	unless ( $self->has_chrom_sizes ){
		$self->_set_chrom_sizes ($self->dbGetter->chromosome_sizes);
	}
};

has gene_list => (
	is => 'ro',
	isa => 'ArrayRef',
	required => 1,
	default => sub{
		die("A gene list has not been set. Please define this attribute upon instantiation.");
	}
);

has valid_genes => (
	is => 'ro',
	isa => 'ArrayRef',
	predicate => 'has_valid_genes',
	writer => '_set_valid_genes'
);

has invalid_genes => (
	is => 'ro',
	isa => 'ArrayRef',
	predicate => 'has_invalid_genes',
	writer => '_set_invalid_genes'
);

before 'valid_genes' => sub {
	my $self=shift;
	unless($self->has_valid_genes){
		my @valid_genes = ();
		my @invalid_genes = ();
		for (@{$self->gene_list}) {
			my $name = $_;
			if (exists $self->genome_bed->{$name}){
				push(@valid_genes,$name);
			}
			if (!(exists $self->genome_bed->{$name})){
				push(@invalid_genes,$name);
			}
		}
		$self->_set_valid_genes(\@valid_genes);
		$self->_set_invalid_genes(\@invalid_genes);
	}
};

has data_hash => (
	is => 'ro',
	isa => 'HashRef',
	predicate => 'has_data_hash',
	writer => '_set_data_hash'
);

before 'data_hash' => sub {
	my $self = shift;
	unless ($self->has_data_hash){
		my $data_hash_ref = {};
		$data_hash_ref->{tss}=[];
		$data_hash_ref->{tts}=[];
		$self->_set_data_hash ($data_hash_ref);
		$self->calcCoords;
	}
};

#Given an array ref of data for a single gene, calculate and return desired coordinates
sub calcCoords
{
        #Assign variable name to input array of gene information.
        my $self = shift;
        my $counter = 0;
        for (@{$self->valid_genes}) {
        
        my $valid_gene = 0;
        
        my $name = $_;

        my $dataArray = $self->genome_bed->{$name};

        #Define gene data
        my $chr = $dataArray->[0];
        my $start = $dataArray->[1];
        my $stop = $dataArray->[2];
        my $strand = $dataArray->[5];
        

        
       my $chrom_length = $self->chrom_sizes->{$chr};
                
        #Get the length of the gene
        my $length = ($stop - $start);
        my $even = $length % 2 == 0;
        
        my $upStart=0;
        my $downStart=0;
        my $upStop=0;
        my $downStop=0;
        
        my $extension_length = 0;
        
        #Set the extension length in bp based on the previously set preferences for either bp or perc distance
        
        #Check for agreement between bp and percentage booleans
        if (($self->percent_distance && $self->bp_distance) ||(!$self->percent_distance && !$self->bp_distance)){
        	message {"Both indicator attributes for percentage and bp distance measurements have been set to the same value. Defaulting to percentage distance measurement."};
        	$self->_set_percent_distance(1);
        	$self->_set_bp_distance(0);
        }
        
        #If the percent distance is set to true
        if ($self->percent_distance){
        	
        	#This accounts for the case of an odd length gene.
        	$extension_length = floor($length*($self->valid_percent));

        }
        if ($self->bp_distance){
        	$extension_length = $self->valid_bp;
        }
        
        $upStart = $start - $extension_length;
        $downStart = $start + $extension_length;
		$upStop = $stop - $extension_length;
        $downStop = $stop + $extension_length;

        
        #Account for corner cases where percent value is .5
        if ($self->percent_distance && ($self->valid_percent == .5)){
        	#If the gene length is even
        	if ($even){
        		
        		#Shorten the 3' upstream coordinate by one.
        		$upStop++;
        		#Shorten the 3' downstream coordinate by one.
        		$downStop--;	
        	}
        }
        
        #If calculated upstream start coordinate is less than zero, zero it.
        if ($upStart < 0)
        {
                $upStart = 0;
        }
        #If calculated downstream end coordinate exceeds the length of the chromosome, truncate it   
        if ($downStop > $chrom_length)
        {
                $downStop = $chrom_length;
        }
        
        #Run checks to ensure this gene has a length that fits the specifications of the user
        #If the coordinate upstream of the 3' end is greater than the coordinate downstream of the 5' end and the full length of the gene is greater than or equal to
        #twice the extension length, then set the valid_gene indicator to 1. Otherwise, it will remain at 0 and will not be pushed to the data array
        if (($upStop > $downStart)&&(($stop-$start) >= 2*$extension_length)){
        	$valid_gene = 1;
        }
        
        #Check that gene has been found valid
        if ($valid_gene){
        	if ($strand eq "+"){
        		push(@{$self->data_hash->{tss}},[$chr, $upStart,$downStart]);
        		push(@{$self->data_hash->{tts}},[$chr, $upStop,$downStop]);
        	}
        	if ($strand eq "-"){
        		push(@{$self->data_hash->{tts}},[$chr, $upStart,$downStart]);
        		push(@{$self->data_hash->{tss}},[$chr, $upStop,$downStop]);
        	}
        }else{
        	push($self->invalid_genes, $name);
        }
        
        
        }
}
        

1; # End of splitter