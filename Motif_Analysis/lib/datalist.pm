package datalist;
use Moose;
use FindBin;
use lib "$FindBin::Bin/../lib";
use Data::Dumper;

#Create two attributes for hashes to store chromosome coordinates. Hashes will be used to filter out duplicate
has 'fivePCoords' => (
	is => 'rw',
	isa => 'HashRef[Str]'
	);
	
has 'threePCoords' => (
	is => 'rw',
	isa => 'HashRef[Str]'
	);

sub createEntry
{
	my $self = shift;
	my $dataArray = $_;
	
	my $fivePEntry = $dataArray->[0] . ":" . $dataArray->[1] . "-" . $dataArray->[2];
	my $threePEntry = $dataArray->[0] . ":" . $dataArray->[3] . "-" . $dataArray->[4];
	
	$self->fivePCoords->$fivePEntry;
	$self->threPCoords->$threePEntry;
	
}

1; # End of datalist
