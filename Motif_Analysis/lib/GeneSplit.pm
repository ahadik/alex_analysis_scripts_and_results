package GeneSplit;
use Moose;
use FindBin;
use lib "$FindBin::Bin/../lib";
use Data::Dumper;
use Carp;
use Moose::Util::TypeConstraints;
use File::Temp qw(tempfile);
use File::chmod;
use splitter;
use IPC::Run3; 
use File::Basename;
use File::Which;



with 'MooseX::Getopt';

subtype 'valid_percentage', as 'Num', where {$_ <= .5 && $_ > 0},
message {"$_ is not a valid percentage. Valid percentages must be greater than 0 and less than .5"};

has percent => (
	is => 'ro',
	isa => 'valid_percentage',
	documentation => 'Set this value to provide gene distances as a percentage of the gene length',
);

has nmotifs => (
	is => 'ro',
	isa => 'Int',
	documentation => 'Optionally set this value to instruct MEME to find up to this many motifs instead of the default of 1.',
	predicate => 'has_nmotifs'
);

has maxw => (
	is => 'ro',
	isa => 'Int',
	documentation => 'Optionally set this value to instruct MEME to find motifs only as large as this value.',
	predicate => 'has_maxw'
);

has minw => (
	is => 'ro',
	isa => 'Int',
	documentation => 'Optionally set this value to instruct MEME to find motifs at least as large as this value.',
	predicate => 'has_minw'
);

has mod => (
	is => 'ro',
	isa => 'Str',
	documentation => 'Optionally set this value to either oops, zoops, or anr.',
	predicate => 'has_mod'
);
has basepair => (
	is => 'ro',
	isa => 'Int',
	documentation => 'Set this value to provide gene distances in terms of a static basepair value.',
);

has genome => (
	is => 'ro',
	isa => 'Str',
	documentation => 'A required argument, this defines the desired genome.',
	required => 1,
	default => sub {
		croak("\n\nA genome must be provided be set to use GeneSplit.\n\n");
	},
);

has gene_list => (
	is => 'ro',
	isa => 'Str',
	documentation => 'This requred argument provides a file path to the list of desired genes.',
	required => 1,
	default => sub {
		croak("\n\nA file path for a gene list must be provided to use GeneSplit\n\n");
	},
);

has genome_2bit => (
	is => 'ro',
	isa => 'Str',
	documentation => "This required argument defines a file path to a 2bit file for the desired genome",
	default => sub {
		croak("\n\nA file path for a .2bit file must be provided\n\n");
	}
);

has output_dir => (
	is => 'ro',
	isa => 'Str',
	documentation => 'This argument defines where the 2BitToFa output files will be stored. If it is not set by the user, the FASTA files will be stored as temp files.',
	predicate => 'has_output_dir'
);

has fa_output => (
	is => 'ro',
	isa => 'Bool',
	documentation => 'This Boolean is set if the user wants to keep the Fasta files generated from 2BittoFa.',
	predicate => 'has_fa_output'
);

has job_name => (
	is => 'ro',
	isa => 'Str',
	documentation => 'This argument defines a name for this job so that output files may be given a unique name.',
	predicate => 'has_job_name'
);

has np => (
	is => 'ro',
	isa => 'Num',
	documentation => 'This argument sets the number of processors for MEME to use for parallel runs. Note that the local installation of MEME msut be configured for parallel processing by compiling it with the mpicc installed.',
	predicate => 'has_np'
);

has maxsize => (
	is => 'ro',
	isa => 'Num',
	documentation => 'This argument is set as an argument to MEME for the maximum dataset size in characters. It should be set by the user only if requested by MEME.',
	predicate => 'has_maxsize',
	writer => '_set_maxsize'
);

has twobit_path	=>	(
	is		=>	'ro',
	isa		=>	'Str',
	default		=>	sub	{
		my $self = shift;
		return which('twoBitToFa');
	},
);

before 'maxsize' => sub {
	my $self = shift;
	unless( $self->has_maxsize ){
		$self->_set_maxsize (100000);
	}
};

sub execute {
	my $self = shift;
	
	if ($self->percent && $self->basepair){croak("\n\nYou have set a value for both basepair and percentage. Only one value is allowed to be set.\n\n")}
	
	#Open the gene list and parse each line into the array
	open my $file, "<", $self->gene_list;
	my @geneList = <$file>;
	close $file;
	chomp(@geneList);

	my $splitter;

	#Instantiate an instance of splitter with the genome and gene list provided by the user.
	if ($self->percent){
		$splitter = splitter->new(genome => $self->genome, gene_list => \@geneList, percent_distance => 1, distance => $self->percent);
	}else{
		if ($self->basepair){
			$splitter = splitter->new(genome => $self->genome, gene_list => \@geneList, bp_distance => 1, distance => $self->basepair);
		}else{
			$splitter = splitter->new(genome => $self->genome, gene_list => \@geneList);
		}
	}
	
	my $tss_coord_list;
	my $tss_filename;
	my $tts_coord_list;
	my $tts_filename;

	#If the user has specified the fa_output option, create files to store the coordinates in. Otherwise, create temp files.
	if ($self->has_fa_output){
		if ($self->has_job_name){
			$tss_filename = $self->output_dir.$self->job_name."_tss.2bit";
			$tts_filename = $self->output_dir.$self->job_name."_tts.2bit";
		}else{
			$tss_filename = $self->output_dir."motifAnalysis_tss.2bit";
			$tts_filename = $self->output_dir."motifAnalysis_tts.2bit";
		}
		open($tss_coord_list, ">", File::Spec->rel2abs($tss_filename)) or die "Could not open file '$tss_filename' $!";
		open($tts_coord_list, ">", File::Spec->rel2abs($tts_filename)) or die "Could not open file '$tts_filename' $!";
	}else{
		($tss_coord_list,$tss_filename) = tempfile(SUFFIX => '.2bit');
		($tts_coord_list,$tts_filename) = tempfile(SUFFIX => '.2bit');
	}

	chmod(0755, $tss_filename);
	chmod(0755, $tts_filename);

	my $abs_tss_path = File::Spec->rel2abs($tss_filename);
	my $abs_tts_path = File::Spec->rel2abs($tts_filename);

	#For every element in the tss hash, print it to a temp file
	for (@{$splitter->data_hash->{tss}}){

		my @gene_info = $_;
		print $tss_coord_list $gene_info[0][0].":".$gene_info[0][1]."-".$gene_info[0][2]."\n";
	}
	close $tss_coord_list;

	
	for (@{$splitter->data_hash->{tts}}){
		my @gene_info = $_;

		print $tts_coord_list $gene_info[0][0].":".$gene_info[0][1]."-".$gene_info[0][2]."\n";
	}
	close $tts_coord_list;
	
	#Create file for tss sites
        my $tss_output_file;
        my $tss_output;
        
        #If the user set an output directory, set the output file to a .fa file for that directory, otherwise create a temp file to write to.
        if ($self->has_fa_output){
        	if ($self->has_job_name){
        		$tss_output_file = $self->output_dir.$self->job_name."_tss.fa";
        	}else{
			$tss_output_file = $self->output_dir."motif_analysis_tss.fa";
		}
        }else{
                ($tss_output, $tss_output_file) = tempfile(SUFFIX => '.fa');
        }

	my $abs_genome_2bit = File::Spec->rel2abs($self->genome_2bit);
	my $abs_tss_output_file = File::Spec->rel2abs($tss_output_file);

	#my @tss_cmd = ("twoBitToFa", $abs_genome_2bit, $abs_tss_output_file,  "-seqList=$abs_tss_path",);
        my @tss_cmd = ($self->twobit_path, $abs_genome_2bit, $abs_tss_output_file,  "-seqList=$abs_tss_path",);
       
        #Define a scalar for std_err
        my $tss_stderr = 0;
<<<<<<< HEAD
       
print Dumper(@tss_cmd);
 
=======

	print Dumper(@tss_cmd);

>>>>>>> ae88d9e3b60d9b4735c759b527addd2421c5e720
        run3 \@tss_cmd, undef, undef, \$tss_stderr;
	
        #If twoBitToFa throws an error, catch it in the string tss_stderr and kill the process, printing the error.
        if ($tss_stderr) {
        	croak "\ntwoBitToFa Error:\n\n$tss_stderr\n\n";
        }
        
        #Create file for tts sites
        my $tts_output_file;
        my $tts_output;
        
        #If the user set an output directory, set the output file to a .fa file for that directory, otherwise create a temp file to write to.
        
        if ($self->has_fa_output){
        	if ($self->has_job_name){
        		$tts_output_file = $self->output_dir.$self->job_name."_tts.fa";
        	}else{
                $tts_output_file = $self->output_dir."motifAnalysis_tts.fa";
	}
        }else{
                ($tts_output, $tts_output_file) = tempfile(SUFFIX => '.fa');
        }

        my @tts_cmd = ($self->twobit_path, $self->genome_2bit, $tts_output_file,  "-seqList=$tts_filename",);
        
        #Define a scalar for std_err
        my $tts_stderr=0;
        
        run3 \@tts_cmd, undef, undef, \$tts_stderr;    
        
        #If twoBitToFa throws an error, catch it in the string tss_stderr and kill the process, printing the error.
        if ($tts_stderr) {
        	croak "\ntwoBitToFa Error:\n\n$tts_stderr\n\n";
        }
        
        #Print list of genes that were not computed
        print "\nThe following genes were not computed or reported as they were not referenced in the BED file.\n\n";
        for (@{$splitter->invalid_genes}){
                print $_."\n";
        }
        
        #Generate success message
        print "\nFASTA files successfully generated. Computing common motifs using MEME.\n\n";
        
        #Instantiate arrays for meme commands
        my @meme_tss_cmd;
        my @meme_tts_cmd;
        
        #Generate meme command with parallel option set if the user has specified a number of processors
        if ( $self->has_np ){
        	@meme_tss_cmd = ("meme", $tss_output_file, "-o", $self->output_dir.$self->job_name."_tss/", "-dna", "-maxsize", $self->maxsize, "-p", $self->np);
        	@meme_tts_cmd = ("meme", $tts_output_file, "-o", $self->output_dir.$self->job_name."_tts/", "-dna", "-maxsize", $self->maxsize, "-p", $self->np);
        }else{
        	@meme_tss_cmd = ("meme", $tss_output_file, "-o", $self->output_dir.$self->job_name."_tss/", "-dna", "-maxsize", $self->maxsize);
        	@meme_tts_cmd = ("meme", $tts_output_file, "-o", $self->output_dir.$self->job_name."_tts/", "-dna", "-maxsize", $self->maxsize);
        }
       
	print Dumper(@meme_tss_cmd);
	print Dumper(@meme_tts_cmd);

        
        #Append option commands to meme call if they have been specified by the user
        if ( $self->has_maxw){
        	push(@meme_tss_cmd,"-maxw");
        	push(@meme_tss_cmd,$self->maxw);
        	push(@meme_tts_cmd,"-maxw");
        	push(@meme_tts_cmd,$self->maxw);
        }
<<<<<<< HEAD
       
	print Dumper(@meme_tss_cmd);
	print Dumper(@meme_tts_cmd);
 
=======
        
        if ( $self->has_minw){
        	push(@meme_tss_cmd,"-minw");
        	push(@meme_tss_cmd,$self->minw);
        	push(@meme_tts_cmd,"-minw");
        	push(@meme_tts_cmd,$self->minw);
        }
        #If nmotifs is not provided, default to 1.
        if ( $self->has_nmotifs){
        	push(@meme_tss_cmd,"-nmotifs");
        	push(@meme_tss_cmd,$self->nmotifs);
        	push(@meme_tts_cmd,"-nmotifs");
        	push(@meme_tts_cmd,$self->nmotifs);
        }else{
        	push(@meme_tss_cmd,"-nmotifs");
        	push(@meme_tss_cmd,1);
        	push(@meme_tts_cmd,"-nmotifs");
        	push(@meme_tts_cmd,1);
        }
        
        if ( $self->has_mod){
        	push(@meme_tss_cmd,"-mod");
        	push(@meme_tss_cmd,$self->mod);
        	push(@meme_tts_cmd,"-mod");
        	push(@meme_tts_cmd,$self->mod);
        }

>>>>>>> ae88d9e3b60d9b4735c759b527addd2421c5e720
        #Run the meme commands
        run3 \@meme_tss_cmd, undef, undef, undef;
        run3 \@meme_tts_cmd, undef, undef, undef;
        
        print "\nSuccessfully computed motifs for tss and tts across gene list. Output files have been stored in ".$self->output_dir." \n\n";
        
}

1;
