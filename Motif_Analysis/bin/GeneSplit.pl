#!/usr/bin/perl -w
# #!/usr/bin/env perl - allows for the use of a perl that is not a shared
# installation. Also no need for the -w flag because you have said 'use
# warnings;' below.

use strict;
use warnings;
# use Modern::Perl;
# This will give you strict, warnings, and a bunch of other extremely convenient
# imports
use Data::Dumper;
use File::Which;
use FindBin;
use lib "$FindBin::Bin/../lib";
use GeneSplit;

my $GeneSplit = GeneSplit->new_with_options();
	$GeneSplit->execute;
