#! /bin/bash

cd /gpfs/data/larschan/jdobson/working_directory/RNA-seq/Analysis/07-01-13/

for directory in *
do

	cd $directory
	pwd

	echo "Parsing isoform_exp"
	perl /gpfs/data/larschan/jdobson/working_directory/RNA-seq/Analysis/filter_gene_list.pl isoform_exp.diff 2 $directory

	cd ..

done