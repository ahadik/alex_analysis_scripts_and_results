#! /bin/bash

cd /gpfs/data/larschan/jdobson/working_directory/RNA-seq/Analysis/07-01-13/

for directory in *
do

	cd $directory
	pwd

	echo "Filter isoform_exp"
	perl /gpfs/data/larschan/jdobson/working_directory/RNA-seq/Analysis/isoform_filter.pl isoform_exp.diff 2.0 $directory

	cd ..

done