#!/usr/bin/perl -w

#This perl script produces a CSV file listing genes filtered from an input .diff file from CuffDiff. 
#Genes are filtered by their log_2 fold change which is input as an argument. All genes whose fold change fall below this argument are listed in the output.
#Genes are listed by their RefSeq id.

use strict;
use warnings;
use Data::Dumper;

#Define the number of arguments provided to the script
my $argnum = $#ARGV + 1;

if ($ARGV[0] eq "help"){
	print "\nHELP DOCUMENTATION FOR FILTER_GENE_FOLD_CHANGE\n\nPURPOSE: Convert present differential expression data in .csv file after filtering with an upper fold change bound.\n\n";
	print "INPUT: Three arguments are expected.\n\n";
	print "file: path to isoform_exp.diff output file from cuffdiff algorithm.\n";
	print "logBound: the upper bound to filter by, in log_2 form.\n";
	print "fileSample: The name of the sample being worked on to be used in the output file name.\n\n";
	print "OUTPUT:\nfiltered gene list: A .csv file listing the RefSeq IDs for all genes whose fold change falls below the provided upper bound.\n";
	
	exit 1;
	
}

#Check that only one file is entered for parsing
if ($#ARGV != 2)
{
	die "$argnum arguments were received, but 3 arguments are expected";
}

#Define the file path from the provided argument
my $file = $ARGV[0];

#Define the bound to filter by
my $logBound = $ARGV[1];
my $bound = log($logBound)/log(2);
$bound = -$bound;

#Define the sample from the provided name argument
my $sample = $ARGV[2];

my $fileSample = '>>' . $sample . '-bound' . $bound .'.csv';

#Instantiate an array for holding gene IDs that meet the provided bound
my @genes;

#Define a file handle, $fh, to represent the opened file provided as an argument
open (my $fh , "<", $file)
#If the file cannot be opened, die and give a reason.
 or die "Couldn't open $file : $!";

#Open file OUTPUT with name fold-change.csv
open (OUTPUT, $fileSample);

#For each line in the file
while (<$fh>) {
	my $line = $_;
	
	#Remove the new line character
	chomp($line);

	#If the line does not begin with test_id
	unless ($line =~ /^test_id/){
		
		#Split the line on tabs
		my @line_items = split(/\t/,$line);
		
		#Define the test_id of the gene for reference
		my $geneID = $line_items[0];
		
		if ($line_items[9] < $bound)
		{
			print $geneID . "\n";
			print OUTPUT $geneID . "\n";
		}
		
	};
	
}



close (OUTPUT);