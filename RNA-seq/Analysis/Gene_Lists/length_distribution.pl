#!/usr/bin/perl -w

#THIS FILE IS DESIGNED OUTPUT A .CSV FILE THAT PARSES FROM A BED FILE THE LENGTH OF GENES

use strict;
use warnings;
use Data::Dumper;

#Define the number of arguments provided to the script
my $argnum = $#ARGV + 1;

#Check that only one file is entered for parsing
if ($#ARGV != 1)
{
	die "$argnum arguments were received, but 2 arguments are expected";
}

#Define the file path from the provided argument
my $file = $ARGV[0];

#Define the gene list that will filter output values
my $geneList = $ARGV[1];

#Initiate an array for holding arrays of info for active genes
my %geneLengths;

#Define a file handle, $fh, to represent the opened file provided as an argument
open (my $fh , "<", $file)
#If the file cannot be opened, die and give a reason.
 or die "Couldn't open $file : $!";

#For each line in the file
while (<$fh>) {
	my $line = $_;
	
	#Remove the new line character
	chomp($line);
		
	#Split the line on tabs
	my @line_items = split(/\t/,$line);

	my $length = ($line_items[2] - $line_items[1]);
	my $refSeq = $line_items[3];	
	
	$geneLengths{$refSeq} = $length;
	
}

#Initiate an array for holding all genes in gene list
my @geneList;

#Define a file handle, $genes, to represent the opened gene list provided as an argument
open (my $genes , "<", $geneList)
#If the file cannot be opened, die and give a reason.
 or die "Couldn't open $file : $!";
 
 
 #Open file OUTPUT with name unregulated_xChrom.csv
open (OUTPUT, '>>gene_length' . $geneList . '.csv');
 
 #For each line in the file
while (<$genes>) 
{
	my $line = $_;
	
	push @geneList, $line;
	
	#Remove the new line character
	chomp($line);
	
	if (exists $geneLengths{$line})
	{
	print OUTPUT $line . "," . $geneLengths{$line} . "\n";
	}
	
}

close (OUTPUT);

my %nonResponse = map { $_ => 1 } @geneList;

#Open file OUTPUT with name unregulated_xChrom.csv
open (NONRESP_OUTPUT, '>>gene_length_non-response' . $geneList . '.csv');

foreach my $key ( keys %geneLengths )
{
	#If the key from geneLengths isn't in the nonResponse hash, then it wasn't inserted from the bounded gene list so it's not responsive
  if (!$nonResponse{$key})
  {
  	print NONRESP_OUTPUT $key . "," . $geneLengths{$key} . "\n";
  }
}

close (NONRESP_OUTPUT);