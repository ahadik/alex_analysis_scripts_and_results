#! /bin/bash

cd ~/data/jdobson/working_directory/RNA-seq/Analysis/Gene_Lists/parsed-data

for directory in *
do

	cd $directory

	rm *.csv.csv

	perl ~/data/jdobson/working_directory/RNA-seq/Analysis/Gene_Lists/length_distribution.pl ~/scratch/working/coordinates.txt *-gene_list_-1-2_fold-change.csv

	perl ~/data/jdobson/working_directory/RNA-seq/Analysis/Gene_Lists/length_distribution.pl ~/scratch/working/coordinates.txt *-gene_list_-2_fold-change.csv

	cd ..

done
