#Differential Expression Gene Filtering
##Gene List and Data Set
###Purpose
To filter the output of *cuffdiff* by a log_2 fold change uppoer bound. The data will be filtered from a tab-delimited .diff file to a .csv file.

###Scripts
####Gene List
		filter_gene_id_list.pl
#####Use
*	Input three command line arguments
	*	file: Path to isoform_exp.diff output file from *cuffdiff* algorithm.
	*	logBound: Upper log_2 bound to filter gene data by.
	*	fileSample: Name of the sample being filtered. Used for naming purposes.
*	Output a .csv file containing a list of RefSeq IDs for genes present in the isoform_exp.diff file whose log_2 fold change fall below the upper logBound.
#####Execution
		filter_gene_id_list_execution.sh
		
#####Use
A shell script that traverses a list of directories and executes
		
	filter_gene_id_list.pl
in each directory.