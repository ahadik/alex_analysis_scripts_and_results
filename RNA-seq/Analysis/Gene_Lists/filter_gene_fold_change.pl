#!/usr/bin/perl -w

use strict;
use warnings;
use Data::Dumper;

#Define the number of arguments provided to the script
my $argnum = $#ARGV + 1;

if ($ARGV[0] eq "help"){
	print "\nHELP DOCUMENTATION FOR FILTER_GENE_FOLD_CHANGE\n\nPURPOSE: Convert present differential expression data in .csv file after filtering with an upper fold change bound.\n\n";
	print "INPUT: Three arguments are expected.\n\n";
	print "file: path to isoform_exp.diff output file from cuffdiff algorithm.\n";
	print "logBound: the upper bound to filter by, in log_2 form.\n";
	print "fileSample: The name of the sample being worked on to be used in the output file name.\n\n";
	print "OUTPUT:\nfiltered gene list data set: A .csv file listing the RefSeq ID, gene name, chromosome, rpkm values, folding change, and p Value for all genes whose fold change falls beneath the provided upper bound.\n";
	
	exit 1;
	
}

#Check that only one file is entered for parsing
if ($#ARGV != 2)
{
	die "$argnum arguments were received, but 3 arguments are expected";
}

#Define the file path from the provided argument
my $file = $ARGV[0];

#Define the bound to filter by
my $logBound = $ARGV[1];
my $bound = log($logBound)/log(2);
$bound = -$bound;

#Define the sample from the provided name argument
my $sample = $ARGV[2];

my $fileSample = '>>' . $sample . '-data-' . $bound . '-bound.csv';

#Instantiate a multi-demensional array for holding gene fold changes in arrays of chromosomes
my @genes = (my @testID, my @geneID, my @chrom, my @rpkm1, my @rpkm2, my @foldChange, my @pVal);

#Define a file handle, $fh, to represent the opened file provided as an argument
open (my $fh , "<", $file)
#If the file cannot be opened, die and give a reason.
 or die "Couldn't open $file : $!";

my @header;

#For each line in the file
while (<$fh>) {
	my $line = $_;
	
	#Remove the new line character
	chomp($line);
	
	#If this is the first line of the file, then construct an array to represent the headers
	if ($line =~ /^test_id/){
		#Split the line on tabs
		my @headers = split(/\t/,$line);
		
		push (@header, $headers[0]);
		push (@header, $headers[1]);
		push (@header, $headers[3]);
		push (@header, $headers[7]);
		push (@header, $headers[8]);
		push (@header, $headers[9]);
		push (@header, $headers[11]);
		
	}
	
	#If the line does not begin with test_id
	unless ($line =~ /^test_id/){
		
		#Split the line on tabs
		my @line_items = split(/\t/,$line);
		
		if ($line_items[9] < $bound)
		{
		
		#Define the chromosome name for the gene represented by the current line by splitting on ':'
		#and taking all characters preceding
		my $chr = (split(/:/,$line_items[3]))[0];
		
		#Push test-id to first array
		push @{$genes[0]}, $line_items[0];
		#Push gene-id to second array
		push @{$genes[1]}, $line_items[1];
		#Push chromosome name to third array
		push @{$genes[2]}, $chr;
		#Push PRKM 1 to fourth array
		push @{$genes[3]}, $line_items[7];
		#Push PRKM 2 to fifth array
		push @{$genes[4]}, $line_items[8];
		#Push fold change value to sixth array
		push @{$genes[5]}, $line_items[9];
		#Push p-value to seventh array
		push @{$genes[6]}, $line_items[11];
		}
		
	};
	
}

#Open file OUTPUT with name fold-change.csv
open (OUTPUT, $fileSample);

#Instantiate a string to hold the header line of the CSV
my $headerText = "";

#For every element of the header array
for (my $h=0; $h < @header; $h++)
{
	#append each header to the header string followed by a comma
	$headerText = $headerText . $header[$h] . ",";
}


#Print the header line, with the final comma trimmed off, followed by a new line
#to the output file
print OUTPUT (substr($headerText, 0, -1) . "\n");
print (substr($headerText, 0, -1) . "\n");

#For every index of number of elements in first array of @genes array
my $entryLimit = scalar @{$genes[0]};
for (my $i=0; $i < $entryLimit; $i++)
{
	#Construct a line for the .csv file by traversing through all elements in the current row of @genes
	my $currentLine = "";
	my $geneLimit = $#genes;
	for (my $j=0; $j <= $geneLimit; $j++)
	{
		$currentLine = $currentLine . $genes[$j][$i] . ",";
		
	}
	
	print OUTPUT (substr($currentLine, 0, -1) . "\n");
	print (substr($currentLine, 0, -1) . "\n");
	
}



close (OUTPUT);