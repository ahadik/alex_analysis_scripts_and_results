#!/usr/bin/perl -w

use strict;
use warnings;
use Data::Dumper;

#Define the number of arguments provided to the script
my $argnum = $#ARGV + 1;

if ($ARGV[0] eq "help"){
	print "\nHELP DOCUMENTATION FOR BED-INDEX\n\nPURPOSE: Parse BED formatted data sets and extract genes matched against a gene list \n\n";
	print "INPUT: Four arguments are needed for input. \n\n";
	print "reference file: BED formatted file extracted from model genome. \n";
	print "gene list: a .csv file of the format test_id, gene_id, locus, value_1, value_2, log2(fold_change), p_value \n";
	print "sample: name identifying sample from which gene list originated \n";
	print "bound: a number indicating the bound used to generate the associated gene list. Used for naming output file. \n";
	
	exit 1;
	
}

#Check that four arguments are received
if ($#ARGV != 3)
{
	die "$argnum arguments were received, but 4 arguments are expected";
}

#Define the file path for the dm3 reference index file
my $refFile = $ARGV[0];

#Define the file path for the gene list of genes filtered by fold-change bound
my $geneList = $ARGV[1];

#Define the file name from sample name
my $sample = $ARGV[2];

#Define the bound for naming purposes
my $bound = $ARGV[3];

#Define a file handle, $fh, to represent the opened file provided as an argument
open (my $fh , "<", $refFile)
#If the file cannot be opened, die and give a reason.
 or die "Couldn't open $refFile : $!";
 
 #Define concatenated output file name
 my $fileSample = '>>' . $sample . '-gene-coords_bound-' . $bound . '.bed';

#Define hash to store relevant data
my %data;

#Open file OUTPUT with name fold-change.csv
open (OUTPUT, $fileSample);

my $currentLine;

#For each line in the file
while (<$fh>) {
	my $line = $_;
	
	#Remove the new line character
	chomp($line);
		
	#Split the line on tabs
	my @line_items = split(/\t/,$line);
		
	#insert array of gene data to hash, with key of RefSeq ID
	push( @{$data{$line_items[3]}}, @line_items);
	
}


#Define a file handle, $geneData, to represent opened gene list provided as an argument
open(my $geneData, "<", $geneList)
	#Die gracefully if not able to be opened
	or die "Couldn't open gene list $geneList : $!";
	
#For each gene in the gene data file
while (<$geneData>) {
	
	my $line = $_;
	
	unless ($line =~ /^test_id/){
	
	#Define string to represent line of output file
	my $lineOutput = "";
	
	#Remove the new line character
	chomp($line);
	
	#Split the line on commas
	my @line_items = split(',',$line);
	
	#Define string of refSeq to be the refSeq from the gene dataset
	my $refSeq = $line_items[0];
	
	#Check that the gene listed in the gene data set exists in the reference file
	if (exists $data{$refSeq}){
		#if the gene exists
		
		#Set the geneMatch array value of fold change to equal the fold change listed in the gene data set
		$data{$refSeq}[4] = $line_items[5];

		#For all indices in the array returned from the hash for the gene refSeq
		for(my $i=0;$i<6;$i++)
		{
			#generate a string of text, delineated by tabs, to output to the .bed output file.
			$lineOutput = $lineOutput . $data{$refSeq}[$i] . "\t";
		}
	
		#Remove the last tab, and replace with a new line character
		$lineOutput = (substr($lineOutput, 0, -1) . "\n");
	
		#Print the matched, modified gene to the output file
		print OUTPUT $lineOutput;
		print $lineOutput;		
	}
}
}

close (OUTPUT);