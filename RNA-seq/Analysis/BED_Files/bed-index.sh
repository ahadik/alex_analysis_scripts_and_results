#! /bin/bash

cd /gpfs/data/larschan/jdobson/working_directory/RNA-seq/Analysis/07-01-13/parsed-data/

for directory in *
do

	cd $directory
	pwd

	rm *.bed

	echo "Generating 1.2 filtered BED file"
	perl /gpfs/data/larschan/jdobson/working_directory/RNA-seq/Analysis/bed-index.pl /gpfs/data/larschan/jdobson/working_directory/RNA-seq/Analysis/UCSC_RefSeq_dm3.bed *dataset_-1-2_fold-change.csv $directory 1.2

	echo "Generating 2.0 filtered BED file"
	perl /gpfs/data/larschan/jdobson/working_directory/RNA-seq/Analysis/bed-index.pl /gpfs/data/larschan/jdobson/working_directory/RNA-seq/Analysis/UCSC_RefSeq_dm3.bed *dataset_-2_fold-change.csv $directory 2.0

	cd ..

done