#! /bin/bash

cd /gpfs/data/larschan/jdobson/working_directory/RNA-seq/Analysis/07-01-13/

for directory in *
do

	cd $directory
	pwd

	echo "Parsing gene_exp"
	perl /gpfs/home/ahadik/bin/gene_parse.pl gene_exp.diff

	cd ..

done