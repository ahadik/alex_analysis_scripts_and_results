#!/bin/bash

cd /gpfs/data/larschan/jdobson/working_directory/RNA-seq/Analysis/cuffdiff/07-01-13/

for directory in *
do

	cd $directory
	pwd
	
	rm fold-change.csv

	echo "Parsing gene_exp"
	perl /users/ahadik/data/jdobson/working_directory/RNA-seq/Analysis/Parser/diff_to_csv_parser.pl isoform_exp.diff

	cd ..

done