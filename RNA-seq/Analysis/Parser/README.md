#CuffLinks Data Parser
##Chromosome Name and Associated Fold Changes
###Purpose
To use the output of *cuffdiff* to visualize differential gene expression on the x chromosome vs. the autosome resulting from RNAi experiments. The data will be visualized using data points from a CSV file, which must be generated from the tab-delimited output files of the *cuffdiff* algorithm.

###Scripts
####Parser
		diff_to_csv_parser.pl
		
#####Use

*	Input the path to a single *gene_exp.diff* file from the *cuffdiff* algorithm as a single command line argument.
*	Output a .csv file containing a list of chromosomes and the fold change of genes located on the listed chromsome. Individual gene names are not stored.

####Execution
		recursive_execution.sh
		
#####Use
A shell script that traverses a list of directories and executes
		
	diff_to_csv_parser.pl
in each directory.