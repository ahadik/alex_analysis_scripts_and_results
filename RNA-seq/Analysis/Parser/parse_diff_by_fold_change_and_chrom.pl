#!/usr/bin/env perl 
#===============================================================================
#
#         FILE: parse_diff_by_fold_change_and_chrom.pl
#
#        USAGE: ./parse_diff_by_fold_change_and_chrom.pl  
#
#  DESCRIPTION: This script parses the .diff files produced by Cuffdiff based on
#               the following user-defined inputs:
#                   1. The path to the .diff file
#                   2. The fold-change cutoff
#                   3. The minimum number of RPKM needed to count a gene
#               and produces a folder with files of gene lists by above/below
#               threshold and chromosome.
#
#      OPTIONS: ---
# REQUIREMENTS: Modern::Perl, autodie, Getopt::Long, Pod::Usage, List::Util,
#               File::Basename, FindBin, File::Path
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Jason R. Dobson (), Jason_Dobson@brown.edu
# ORGANIZATION: Center for Computational Molecular Biology
#      VERSION: 1.0
#      CREATED: 08/23/2013 14:37:48
#     REVISION: ---
#===============================================================================

use Modern::Perl;
use autodie;
use Getopt::Long;
use Pod::Usage;
use List::Util qw(sum);
use File::Basename;
use File::Path qw(make_path);
use FindBin;

# Pre-declare variables that will print messages to the user
my $man = 0;
my $help = 0;

# Pre-declare variables for the input values for this script
my $diff_fh = undef;
my $fc_cutoff = undef;
my $min_rpkm = 1;

# Get the command-line options
GetOptions(
    "cuffdiff_file=s"   =>  \$diff_fh,
    "fold_change=f"     =>  \$fc_cutoff,
    "rpkm=f"            =>  \$min_rpkm,
    "help|?"            =>  \$help,
    "man"               =>  \$man,
) or pod2usage(2);

# If help was requested, print a help message
pod2usage(1) if $help;

# If the manual was requested, print the manual
pod2usage(-verbose  =>  2) if $man;

# Make sure that the required options were set
pod2usage(2) unless (defined($diff_fh) && defined($fc_cutoff));

# Pre-declare a Hash Ref to hold the gene lists
my $gene_lists = {};

# Define fold change values
my $lower_fc = -(log(abs($fc_cutoff))/log(2));
my $upper_fc = log(abs($fc_cutoff))/log(2);

# Pre-declare a line number to know which line the file is at
my $line_number = 0;

# Pre-declare a variable that holds the index position of the fold-change value
my $fold_change_index = undef;

# Open the file and iterate through the lines. Use the first line to determine
# at which position the fold change is defined
open my $file, "<", $diff_fh;
while(<$file>) {

    # Increase the line_number
    $line_number++;

    my $line = $_;
    chomp $line;
    
    # If it is the first line, extract the field that contains the fold change
    if ( $line_number == 1 ) {
        my @header_items = split(/\t/, $line);
        for ( my $i = 0; $i < @header_items; $i++ ) {
            if ($header_items[$i] =~ /log2/) {
                $fold_change_index = $i;
            }
        }
    } else {
        # Make sure the fold_change_index has been defiend
        if ( defined ( $fold_change_index ) ) {
            my @line_items = split(/\t/, $line);

            # Check to see if the transcript is above the RPKM defined
            my $number_of_samples = ($fold_change_index-5)/2;
            if ( (sum(@line_items[($fold_change_index - $number_of_samples) ..
                        ($fold_change_index-1)]) / $number_of_samples) >= $min_rpkm)
            {
                # Extract the chromosome
                my ($chr, $coordinates) = split(/:/, $line_items[3]);

                # Determine whether the transcript is responsive and if so in
                # which direction is it responsive
                if ( abs($line_items[$fold_change_index]) >= $upper_fc ) {
                    if ( $line_items[$fold_change_index] >= $upper_fc ) {
                        push(@{$gene_lists->{responsive_up}{$chr}}, $line_items[0]);
                    } else {
                        push(@{$gene_lists->{responsive_down}{$chr}}, $line_items[0]);
                    }
                } else {

                    # Add the transcript as unresponsive
                    push(@{$gene_lists->{unresponsive}{$chr}}, $line_items[0]);
                }
            }
        } else {
            die "\n\nThere was a problem with the formatting of $diff_fh. Could"
            . " not determine which field has the fold change value.\n\n";
        }
    }
}
close $file;

# Print the gene lists to file

# Define a directory to write the files
my ($file_name, $origin_dir, $suffix) = fileparse($diff_fh, '.diff');
my @directories = split(/\//, $origin_dir);
$file_name = $directories[-1] . '_' . $file_name;
my $out_dir = "$FindBin::Bin/Parsed_Files/$file_name/Fold_Change_" . $fc_cutoff;
make_path($out_dir);

# Iterate through the response types
foreach my $response_type ( keys %{$gene_lists} ) {
    
    # Iterate through the chromosomes
    foreach my $chr ( keys %{$gene_lists->{$response_type}} ) {

        # Define a file string to write these genes
        my $out_fh = join('/',
            $out_dir ,
            $file_name . '_' . $fc_cutoff . '-fold_' . $response_type . '_on_' .
            $chr . '.txt'
        );

        # Print to file
        open my $out_file, ">", $out_fh;
        print $out_file join("\n", @{$gene_lists->{$response_type}{$chr}}) if
        defined $gene_lists->{$response_type}{$chr};
        close $out_file;
    }
}

__END__

=head1 NAME

parse_diff_by_fold_change_and_chrom

=head1 AUTHOR

Jason R. Dobson, Center for Computational Molecular Biology, Brown University

L<Jason_Dobson@brown.edu>

=head1 DESCRIPTION

This script is passed the relative path to a diff file produced by cuffdiff, a
floating-point fold change value and optionally a minimum RPKM for the control
sample for the gene to be considered as expressed. This script parses the file
and prints the RefSeq accessions to separate files based on whether the fold
change in gene expression was below the cutoff, within the cutoff range
(unresponsive) or above the cutoff and which chromosome the gene is encoded.

=head1 SYNOPSIS

parse_diff_by_fold_change_and_chrom.pl [options] -cuffdiff_file <FILE>
-fold_change <FLOAT_VAL>

    Options:
    -help               Brief help message
    -man                Full documentation
    -cuffdiff_file      Path to cuffdiff file [REQUIRED]
    -fold_change        Fold-change cutoff [REQUIRED]
    -rpkm               Mimumum RPKM [Default=1]

=head1 OPTIONS

=over 8

=item B<-help>

Print a brief help message and then exits.

=item B<-man>

Print the manual page and then exits.

=item B<-cuffdiff_file>

The relative or full path to the cuffdiff file that is to be parsed. This
argument is required.

=item B<-fold_change>

This floating point value that differentiates 'responsive' from 'unresponsive'
genes. This argument is required.

=item B<-rpkm>

This floating point value is the minimum RPKM for the control sample(s) for the
gene to be considered for defining as 'responsive' or 'unresponsive'. This value
is optional and by default it is set to 1.

=cut
