#!/usr/bin/perl -w

use strict;
use warnings;
use Data::Dumper;

#Define the number of arguments provided to the script
my $argnum = $#ARGV + 1;

#Check that only one file is entered for parsing
if ($#ARGV != 0)
{
	die "$argnum arguments were received, but 1 argument is expected";
}

#Define the file path from the provided argument
my $file = $ARGV[0];

#Instantiate a hash for holding gene fold changes in arrays of chromosomes
my %genes;

#Define a file handle, $fh, to represent the opened file provided as an argument
open (my $fh , "<", $file)
#If the file cannot be opened, die and give a reason.
 or die "Couldn't open $file : $!";

#For each line in the file
while (<$fh>) {
	my $line = $_;
	
	#Remove the new line character
	chomp($line);
	
	#If the line does not begin with test_id
	unless ($line =~ /^test_id/){
		
		#Split the line on tabs
		my @line_items = split(/\t/,$line);
		
		#Define the chromosome name for the gene represented by the current line by splitting on ':'
		#and taking all characters preceding
		my $chr = (split(/:/,$line_items[3]))[0];
		
		#Push the fold change from indice 9 to the hash table of arrays
		#using the kv pair chr/fold-change
		
		#If the chromosome has been previously indexed as a key, the fold change
		#will be added to the end of the array value.
		#If the chromosome has not yet been indexed, a new kv pair
		#will be added to the hash, and the fold change pushed
		push( @{$genes{$chr}}, $line_items[9]);
		
	};
	
}

#Open file OUTPUT with name fold-change.csv
open (OUTPUT, '>>fold-change.csv');

#Instantiate upperLim variable to hold size of largest array in %genes
my $upperLim = 0;

#Instantiate a string to hold the header line of the CSV
my $header = "";

#for every key in the %genes hash
foreach my $key ( keys %genes )
{
	#append the key's value to the header string followed by a comma
	$header = $header . $key . ",";

	#if the length of the array associated with the current key is longer than the 
	#currently stored upperLim, update the value of upperLim to the larger value
	if (scalar(@{$genes{$key}}) > $upperLim ){ $upperLim = scalar(@{$genes{$key}}); }
}

#Print the header line, with the final comma trimmed off, followed by a new line
#to the output file
print OUTPUT (substr($header, 0, -1) . "\n");
print (substr($header, 0, -1) . "\n");

#For incremental values of count from 1 until the size of the array minus 1
for (my $i=0; $i < $upperLim; $i++)
{
	#Instantiate a string to hold the fold change values of the current line
	my $log_line = "";
	
	#For every key in %genes
	foreach my $key ( keys %genes )
	{
		#if the value of count is less than the size of the array associated
		#with the current key
		if ( $i < scalar(@{$genes{$key}}))
		{
			
			#check if indexed value contained 'inf'
			if ((@{$genes{$key}})[$i] =~ m/inf/)
			#if it does, leave the cell blank
			{
				$log_line = $log_line . ",";
			}
			#otherwise, include the indexed data as normal
			else
			{
				$log_line = $log_line . (@{$genes{$key}})[$i] . ",";
			}
		}
		#if value of count is not less than the size of the array associated
		#with the current key
		else
		{
			#append only a comma to indicate a blank cell
			$log_line = $log_line . ",";
		}
		
	}
	
	#print $log_line, with the final comma trimmed off followed by a new line
	#to the output file
	print OUTPUT (substr($log_line, 0, -1) . "\n");
	print (substr($log_line, 0, -1) . "\n");
	
}


close (OUTPUT);