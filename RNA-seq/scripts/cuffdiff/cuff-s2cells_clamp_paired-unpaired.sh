#! /bin/bash

#SBATCH -n 16
#SBATCH -t 168:00:00
#SBATCH --mem=MaxMemPerNode
#SBATCH -J 's2cells_clamp_rnai_paired-unpaired'
#SBATCH -o /users/ahadik/scratch/working/s2cells_clamp_rnai_paired-unpaired.out
#SBATCH -e /users/ahadik/scratch/working/s2cells_clamp_rnai_paired-unpaired.err
#SBATCH --qos='ccmb-condo'
#SBATCH --mail-type=ALL
#SBATCH --mail-user=alexander_hadik@brown.edu

#load modules
module load bowtie2
module load tophat
module load samtools
module load cufflinks

#change directories to scratch directory
cd /gpfs/scratch/ahadik/tophat-output

#create variable referencing annotated genes
GENES=/gpfs/scratch/ahadik/downloads/Drosophila_melanogaster/UCSC/dm3/Annotation/Genes/genes.gtf


#Define available control samples for comparison
KCCELLS_GFP_PAIRED=/gpfs/scratch/ahadik/tophat-output/kccells_gfp_rnai_paired_rep1_s_6/accepted_hits.bam

S2CELLS_GFP_PAIRED=/gpfs/scratch/ahadik/tophat-output/s2cells_gfp_rnai_paired_rep1_s_1/accepted_hits.bam

S2CELLS_GFP_UNPAIRED=/gpfs/scratch/ahadik/tophat-output/s2cells_gfp_rnai_unpaired_rep1_s_6/accepted_hits.bam



cuffdiff -o /gpfs/scratch/ahadik/cuffdiff-output/s2cells_clamp_rnai_paired-unpaired -p 8 -L s2cells_gfp_rnai_rep1,s2cells_clamp_rnai_rep1 $GENES $S2CELLS_GFP_PAIRED,$S2CELLS_GFP_UNPAIRED s2cells_clamp_rnai_paired_rep1_s_2/accepted_hits.bam,s2cells_clamp_rnai_unpaired_rep1_s_7/accepted_hits.bam

cd /gpfs/scratch/ahadik/cuffdiff-output/s2cells_clamp_rnai_paired-unpaired


echo 'CALL MADE: \n cuffdiff -o /gpfs/scratch/ahadik/cuffdiff-output/s2cells_clamp_rnai_paired-unpaired -p 8 -L s2cells_gfp_rnai_rep1,s2cells_clamp_rnai_rep1 $GENES $S2CELLS_GFP_PAIRED,$S2CELLS_GFP_UNPAIRED s2cells_clamp_rnai_paired_rep1_s_2/accepted_hits.bam,s2cells_clamp_rnai_unpaired_rep1_s_7/accepted_hits.bam' > README