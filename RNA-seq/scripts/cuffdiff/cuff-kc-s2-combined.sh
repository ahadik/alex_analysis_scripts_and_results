#! /bin/bash

#SBATCH -n 16
#SBATCH -t 168:00:00
#SBATCH --mem=MaxMemPerNode
#SBATCH -J 's2cells_clamp_and_msl2_rnai_paired_rep1_s_5'
#SBATCH -o /gpfs/data/larschan/jdobson/working_directory/RNA-seq/Analysis/07-09-13/kc_s2_combined_clamp_rnai_paired.out
#SBATCH -e /gpfs/data/larschan/jdobson/working_directory/RNA-seq/Analysis/07-09-13/kc_s2_combined_clamp_rnai_paired.err
#SBATCH --qos='ccmb-condo'
#SBATCH --mail-type=ALL
#SBATCH --mail-user=alexander_hadik@brown.edu

#load modules
module load bowtie2
module load tophat
module load samtools
module load cufflinks

#change directories to scratch directory
cd /gpfs/data/larschan/jdobson/working_directory/RNA-seq/Aligned-Seq/06-20-13

#create variable referencing annotated genes
GENES=/gpfs/scratch/ahadik/downloads/Drosophila_melanogaster/UCSC/dm3/Annotation/Genes/genes.gtf

#Define available control samples for comparison
KCCELLS_GFP_PAIRED=/gpfs/data/larschan/jdobson/working_directory/RNA-seq/Aligned-Seq/06-20-13/kccells_gfp_rnai_paired_rep1_s_6/accepted_hits.bam

S2CELLS_GFP_PAIRED=/gpfs/data/larschan/jdobson/working_directory/RNA-seq/Aligned-Seq/06-20-13/s2cells_gfp_rnai_paired_rep1_s_1/accepted_hits.bam



cuffdiff -o /gpfs/data/larschan/jdobson/working_directory/RNA-seq/Analysis/07-09-13/kc_s2_combined_clamp_rnai_paired -p 8 $GENES $S2CELLS_GFP_PAIRED,$KCCELLS_GFP_PAIRED  s2cells_clamp_rnai_paired_rep1_s_2/accepted_hits.bam,kccells_clamp_rnai_paired_rep1_s_8/accepted_hits.bam

cd /gpfs/data/larschan/jdobson/working_directory/RNA-seq/Analysis/07-09-13/kc_s2_combined_clamp_rnai_paired


echo 'CALL MADE: \n cuffdiff -o /gpfs/data/larschan/jdobson/working_directory/RNA-seq/Analysis/07-09-13/kc_s2_combined_clamp_rnai_paired -p 8 $GENES $S2CELLS_GFP_PAIRED,$KCCELLS_GFP_PAIRED  s2cells_clamp_rnai_paired_rep1_s_2 /accepted_hits.bam,kccells_clamp_rnai_paired_rep1_s_8' > README