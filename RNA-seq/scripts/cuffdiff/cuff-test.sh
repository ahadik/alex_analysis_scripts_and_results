#! /bin/bash

#SBATCH -n 16
#SBATCH -t 168:00:00
#SBATCH --mem=MaxMemPerNode
#SBATCH -J 'tophat-mapping-test'
#SBATCH -o /users/ahadik/scratch/working/cufftest.out
#SBATCH -e /users/ahadik/scratch/working/cufftest.err
#SBATCH --qos=normal
#SBATCH --mail-type=ALL
#SBATCH --mail-user=alexander_hadik@brown.edu

echo cuff-test finish