#! /bin/bash

#SBATCH -n 16
#SBATCH -t 168:00:00
#SBATCH --mem=MaxMemPerNode
#SBATCH -J 'kccells_gfp_rnai_paired_rep1_s_6'
#SBATCH -o /users/ahadik/scratch/working/kccells_gfp_rnai_paired_rep1_s_6.out
#SBATCH -e /users/ahadik/scratch/working/kccells_gfp_rnai_paired_rep1_s_6.err
#SBATCH --qos='ccmb-condo'
#SBATCH --mail-type=ALL
#SBATCH --mail-user=alexander_hadik@brown.edu

#load modules
module load bowtie2
module load tophat
module load samtools
module load cufflinks

#change directories to scratch directory
cd /gpfs/scratch/ahadik/tophat-output

#create variable referencing annotated genes
GENES=/gpfs/scratch/ahadik/downloads/Drosophila_melanogaster/UCSC/dm3/Annotation/Genes/genes.gtf

#Define available control samples for comparison
KCCELLS_GFP_PAIRED=/gpfs/scratch/ahadik/tophat-output/kccells_gfp_rnai_paired_rep1_s_6/accepted_hits.bam

S2CELLS_GFP_PAIRED=/gpfs/scratch/ahadik/tophat-output/s2cells_gfp_rnai_paired_rep1_s_1/accepted_hits.bam

S2CELLS_GFP_UNPAIRED=/gpfs/scratch/ahadik/tophat-output/s2cells_gfp_rnai_unpaired_rep1_s_6/accepted_hits.bam



cuffdiff -o /gpfs/scratch/ahadik/cuffdiff-output/kccells_gfp_rnai_paired_rep1_s_6 -p 8 -L kccells_gfp_rnai_paired_rep1_s_6,kccells_gfp_rnai_paired_rep1_s_6 $GENES $KCCELLS_GFP_PAIRED  kccells_gfp_rnai_paired_rep1_s_6/accepted_hits.bam

cd /gpfs/scratch/ahadik/cuffdiff-output/kccells_gfp_rnai_paired_rep1_s_6


echo 'CALL MADE: \n cuffdiff -o /gpfs/scratch/ahadik/cuffdiff-output/kccells_gfp_rnai_paired_rep1_s_6 -p 8 -L kccells_gfp_rnai_paired_rep1_s_6 $GENES $KCCELLS_GFP_PAIRED  kccells_gfp_rnai_paired_rep1_s_6/accepted_hits.bam' > README