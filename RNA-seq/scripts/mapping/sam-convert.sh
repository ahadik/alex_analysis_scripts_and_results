#! /bin/bash

cd /gpfs/scratch/ahadik/tophat-output

module load samtools

for directory in *
do
	cd $directory
	for file in ./*.bam
	do
		echo $directory$file
		cp $file{,.bak}
		samtools view -h $file > ${file/.bam/.sam}
	done
	cd ..
done

