#! /bin/bash

#SBATCH -n 32
#SBATCH -t 168:00:00
#SBATCH -J 'map_rna-seq'
#SBATCH -o /users/ahadik/scratch/working/map_rna-seq_%a.out
#SBATCH -e /users/ahadik/scratch/working/map_rna-seq_%a.err
#SBATCH --mail-type=ALL

#change directories to working dir
cd /gpfs/scratch/ahadik/working

#load bowtie module
module load bowtie

map_reads() {

	paired=paired
	unpaired=unpaired

	cd paired

	for entry in *.fastq
	do
		bowtie -m 1 /gpfs/data/larschan/bowtie_genomes/d_melanogaster_fb5_22 -S $entry > ${entry%.*}.SAM 

	done

	cd ..
	cd unpaired


	for entry in *.fastq
	do
		bowtie -m 1 /gpfs/data/larschan/bowtie_genomes/d_melanogaster_fb5_22 -S $entry > ${entry%.*}.SAM 

	done

}