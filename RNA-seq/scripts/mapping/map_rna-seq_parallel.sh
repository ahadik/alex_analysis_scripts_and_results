#! /bin/bash

#SBATCH -n 16
#SBATCH -t 48:00:00
#SBATCH -J 'map_rna-seq'
#SBATCH -o /users/ahadik/scratch/working/map_rna-seq_parallel_%a.out
#SBATCH -e /users/ahadik/scratch/working/map_rna-seq_parallel_%a.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=alexander_hadik@brown.edu

#change directories to working dir
cd /gpfs/scratch/ahadik/working

#load bowtie module
module load bowtie

	#function to map RNA read to Melangaster genome in parallel processes

	#starting from working directory, cd into 'paired' directory
	cd paired
	
	#define a function that maps reads to the melanogaster genome and reports the file name as an echo once the BowTie call completes
	map_reads(){
		FILE=$1

		#call bowtie to map reads to melanogaster genome and report unique matches in SAM files with the same name 
		#as the associated .fastq file provided bowtie
		bowtie -m 1 /gpfs/data/larschan/bowtie_genomes/d_melanogaster_fb5_22 -S $FILE > ${FILE%.*}.SAM
		#report an identifing message to the output stream
		message="Successfully mapped: "
		report=$message$entry
		echo $report
	}

	#instantiate two arrays to hold PAIRED PIDS and UNPAIRED PIDS
	PAIRED_PIDS=()
	UNPAIRED_PIDS=()

	#report mapping of PAIRED RNA-Seq data
	echo "Map PAIRED reads:"

	#for all entries in the current directory that are .fastq files
	for entry in *.fastq

	do
		#execute the map_reads function defined above and send to background
		map_reads ${entry} &

		#add the process ID to the associated array
		PAIRED_PIDS+=($!)
	done

	#wait for all processes to finish for this directory
	wait ${PAIRED_PIDS[@]}

	#cd up a level and into the unpaired directory
	cd ..
	cd unpaired
	#report mapping of UNPAIRED RNA-Seq data
	echo "Map UNPAIRED reads:"

	#repeat same process for unpaired directory
	for entry in *.fastq
	do
		map_reads ${entry} &
		UNPAIRED_PIDS+=($!)
	done

	wait ${UNPAIRED_PIDS[@]}