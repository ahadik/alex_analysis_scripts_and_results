#! /bin/bash

#SBATCH -n 16
#SBATCH -t 48:00:00
#SBATCH --mem=32G
#SBATCH -J 'tophat-mapping'
#SBATCH -o /users/ahadik/scratch/working/map_rna-seq_tophat.out
#SBATCH -e /users/ahadik/scratch/working/map_rna-seq_tophat.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=alexander_hadik@brown.edu

#load modules
module load bowtie2
module load tophat
module load samtools

#change directories to paired directory
cd /gpfs/scratch/ahadik/working/

#create variable referencing output directory
OUTPUT=/gpfs/scratch/ahadik/tophat-output/

#create variable for reference genome path
GENOME=/gpfs/scratch/ahadik/downloads/Drosophila_melanogaster/UCSC/dm3/Sequence/Bowtie2Index/genome

#create variable referencing annotated genes
GENES=/gpfs/scratch/ahadik/downloads/Drosophila_melanogaster/UCSC/dm3/Annotation/Genes/genes.gtf

#create variabele referencing the root path of RNAi Seq files
RNAI_ROOT=/gpfs/scratch/ahadik/working/RNA-Seq

#universal counter to keep track of what order TopHat calls are being executed in
COUNTER=0

#INPUT: two arguments, one for each paired end read of RNA-Seq data
#OUTPUT: TopHat output written to defined output directory
tophat-map_paired(){
	#collect arguments into variables
	OUTPUT_FILE=$1
	PAIRED1=$2
	PAIRED2=$3

	#if there is no second argument provided
	if [ "$PAIRED2" = "" ]; then


		#Execute TopHat call with one argument
		#generalized tophat call
		# OUTPUT: path to output directory
		# GENES: annoted gene file
		# GENOME: root path to annotated genome Sequence files
		# PAIRED*: each of the paired end 
		tophat -o $OUTPUT_FILE -G $GENES $GENOME $PAIRED1
	#Otherwise, execute it with two arguments
	else


		tophat -o $OUTPUT_FILE -G $GENES $GENOME $PAIRED1 $PAIRED2
	fi
}

#instantiate array to hold PIDS
PIDS=()

cd RNA-Seq


#For every directory in the current working directory (will be a collection of folders containing each sample type)
for directory in *

#Execute the following commands for each directory
do
	#CD into the directory
	cd $directory
	#Instantiate an array to hold file names
	DIRECTORY_FILES=()
	#For every file in the current directory
	for f in *
	#Execute the following commands
	do
		#Add the file name to DIRECTORY_FILES - the array of file names
		DIRECTORY_FILES[${#DIRECTORY_FILES[@]}]=$f

	done

	OUTPUT_DIR=$OUTPUT$directory
	
	#define full file paths for use in BowTie call
	F1=$RNAI_ROOT/$directory/${DIRECTORY_FILES[0]}


	#If two files were placed into the argument array, call TopHat with both, otherwise call with just one
	if [ ${#DIRECTORY_FILES[@]} -gt 1 ]; then
		F2=$RNAI_ROOT/$directory/${DIRECTORY_FILES[1]}


		tophat-map_paired $OUTPUT_DIR $F1 $F2 &
		#Use the two (hard coded for simplicity - should be generalized later) array entries as arguments to the tophat-map_paired function
		#Send the function call to the background to allow for parallel processing
		PIDS+=($!)
	else

		tophat-map_paired $OUTPUT_DIR $F1 &
		PIDS+=($!)
	fi

	sleep 2

	cd ..
done



wait ${PIDS[@]}

echo "EXECUTION COMPLETE"
